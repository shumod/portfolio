(function ($) {
  $(document).ready(function(){
    $('#mobile-menu-button, #mobile-menu-close').click(function(){
    	$('#mobile-menu-button').toggleClass('show');
    	if($('#mobile-menu').is(':visible')){
    		$('#mobile-menu').hide();
            $('.menu-overlay').hide();
    	}
    	else{
    		$('#mobile-menu').show();
            $('.menu-overlay').show();
    	}
    });

    $('.menu-overlay').click(function(){
        $('#mobile-menu-button').removeClass('show');
        $('#mobile-menu').hide();
        $('.menu-overlay').hide();
    });

  });
})(jQuery);
