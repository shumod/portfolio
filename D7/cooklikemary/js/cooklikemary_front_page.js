(function ($) {
  $(document).ready(function(){

    var mobileSize = 1157;

    // Добавляем классы в любимые рецепты
    if($('#block-views-new-recipe-block-1 .view-id-new_recipe').hasClass('mobile-close')){
        $('#block-views-new-recipe-block-1').addClass('close');
        if($(window).width() < mobileSize){
            $('#block-views-new-recipe-block-1 .mobile-close').hide();
        }
    }
    else if($('#block-views-new-recipe-block-1 .view-id-new_recipe').hasClass('mobile-open')){
        $('#block-views-new-recipe-block-1').addClass('open');
    }

    // При клике на заголовок любимых рецептов - меняем класс и пишем куки
    $('#block-views-new-recipe-block-1 h2').click(function(){
        if($('#block-views-new-recipe-block-1').hasClass('open')){
            $('#block-views-new-recipe-block-1').removeClass('open');
            $('#block-views-new-recipe-block-1').addClass('close');
            $.cookie('is_new_recipe_open', 'close');
            if($(window).width() < mobileSize){
                $('#block-views-new-recipe-block-1 .view-id-new_recipe').hide();
            }
        }
        else if($('#block-views-new-recipe-block-1').hasClass('close')){
            $('#block-views-new-recipe-block-1').removeClass('close');
            $('#block-views-new-recipe-block-1').addClass('open');
            $.cookie('is_new_recipe_open', 'open');
            if($(window).width() < mobileSize){
                $('#block-views-new-recipe-block-1 .view-id-new_recipe').show();
            }          
        }
    });

    // Добавляем классы в блоги
    if($('#block-views-blog-block .view-id-blog').hasClass('mobile-close')){
        $('#block-views-blog-block').addClass('close');
        if($(window).width() < mobileSize){
            $('#block-views-blog-block .views-row-1, #block-views-blog-block .views-row-2, #block-views-blog-block .more-link').hide();
        }
    }
    else if($('#block-views-blog-block .view-id-blog').hasClass('mobile-open')){
        $('#block-views-blog-block').addClass('open');
    }

    // При клике на заголовок блога - меняем класс и пишем куки
    $('#block-views-blog-block h2').click(function(){
        if($('#block-views-blog-block').hasClass('open')){
            $('#block-views-blog-block').removeClass('open');
            $('#block-views-blog-block').addClass('close');
            $.cookie('is_blog_open', 'close');
            if($(window).width() < mobileSize){
                $('#block-views-blog-block .views-row-1, #block-views-blog-block .views-row-2, #block-views-blog-block .more-link').hide();
            }
        }
        else if($('#block-views-blog-block').hasClass('close')){
            $('#block-views-blog-block').removeClass('close');
            $('#block-views-blog-block').addClass('open');
            $.cookie('is_blog_open', 'open');
            if($(window).width() < mobileSize){
                $('#block-views-blog-block .views-row-1, #block-views-blog-block .views-row-2, #block-views-blog-block .more-link').show();
            }         
        }
    });

    // При изменении ширины окна - показываем элементы
    $(window).resize(function() {
        if($(window).width() > mobileSize){
            $('#block-views-new-recipe-block-1 .view-id-new_recipe').show();
            $('#block-views-blog-block .views-row-1, #block-views-blog-block .views-row-2').show();
        }
    });


  });
})(jQuery);
