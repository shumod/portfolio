<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */

global $user;
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print $top_gallery; ?>
  <div class="recipe-characteristics">
    <div class="user-picture-min"><?php print $user_picture_min; ?></div>
    <div class="user-name" itemprop="author"><?php print $user_name; ?></div>
    <table>
      <tr>
        <td title="Время нарезки">
          <?php 
          $cutting_time = 0;
          if(isset($content['field_recipe_cutting'])){ 
            $cutting_time = $content['field_recipe_cutting']['#items'][0]['value'];
            print '<span itemprop="cookTime" content="PT'.$cutting_time.'M">' . render($content['field_recipe_cutting']) . '</span>'; 
          }
          ?>
        </td>
        <td title="Время готовки">
          <?php 
            $cook_time = 0;
            $cook_time = $content['field_recipe_cooking']['#items'][0]['value'];
            print '<span itemprop="cookTime" content="PT'.$cook_time.'M">' . render($content['field_recipe_cooking']) . '</span>'; 
          ?>
        </td>
        <td title="Сложность рецепта"><?php print render($content['field_recipe_complexity']); ?></td>
        <td title="На какое количество человек расчитан рецепт"><?php print '<span itemprop="recipeYield">'.render($content['field_recipe_persons_count']) . '</span>'; ?></td>
      </tr>
    </table>
    <?php $total_time = $cook_time + $cutting_time; ?>
    <span itemprop="totalTime" content="PT<?php print $total_time; ?>M"></span>
    <div class="flag-wrapper-flag-count" title="Добавить рецепт в избранное">
      <?php print $flag_link; ?>
      <div class="flag-favorites-count">
        <?php print $likes_count; ?>
      </div>
    </div>
  </div>

  <span itemprop="recipeInstructions">
    <?php print render($content['body']); ?>
  </span>

  <div class="recipe-wrapper">
    
    <div class="left-recipe-sidebar">
      <div class="ingredients">
        <h2>Ингредиенты</h2>
        <div class="ingredients-description">
          <?php print render($content['field_recipe_ingredient_comment']); ?>
        </div>
        <div class="ingredients-table">
          <div class="ingredients-checkbox-info">Есть дома в наличии, отметить:</div>
          <?php print $ingredients_table; ?>
          <div class="ingredients-buy">Не забыть купить</div>
          <div class="ingredients-print">
            <a href="#">Печать</a>
          </div>
        </div>
      </div>

      <div class="recipe-tags"><?php print $recipe_tags; ?></div>
    </div>

    <div class="main-recipe-content">

      <div class="social-links">
        <?php print $print_button; ?>
        <?php print $social_links; ?>
      </div>
      <h2><span>Как готовить</span></h2>
      <div class="recipe-steps">
        <?php print render($content['field_video']); ?>
        <?php print render($content['field_recipe_youtube']); ?>
        <?php 
          foreach ($recipe_steps as $key => $step) {
            $print_key = $key + 1;
            print '<div class="step-key">'. $print_key .'</div>';
            print '<div class="step-text">'. $step["text"] .'</div>';
            print '<div class="step-image">'. $step["image"] .'</div>';
          }
        ?>
      </div>

      <?php
        // Выводим блок рекомендованного
        $block = block_load('views', 'new_recipe-block_4');
        $blocks = _block_render_blocks(array($block));
        $blocks_build = _block_get_renderable_array($blocks);
        echo drupal_render($blocks_build);
      ?>

    </div>

  </div>

  <div class="clearfix"></div>

  <div class="comments noprint">
    <?php 
      print render($content['comments']); 

      global $user;
      if($user->uid == 0){
        print '<div class="register-anonimous">
                 Зарегистрируйтесь, чтобы оставлять комментарии
                 <div class="register-link">
                   <a href="/user/register">Зарегистрироваться</a>
                 </div>
               </div>';
      }
    ?>
  </div>

  <?php print $social_comments; ?>

</div>