<div id="container">
  <header class="noprint">
    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
    <?php endif; ?>
    <span id="slogan"><?php print $site_slogan; ?></span>
    <div id="mobile-menu-button">Моб меню кнопка</div>
    <div id="mobile-menu">
      <div id="mobile-menu-close">Моб меню закрыть</div>
      <?php print render($page['header']); ?>
      <nav id="main-menu" class="navigation">
        <?php print theme('links__system_main_menu', array(
              'links' => $main_menu,
              'attributes' => array(
              'id' => 'main-menu-links',
              'class' => array('links', 'clearfix'),
              ),
        )); ?>
      </nav> <!-- /#main-menu -->
    </div>
  </header>
    <?php print $breadcrumb; ?>
    <?php if (isset($page['sidebar_first'])): ?>
    <aside><?php print render($page['sidebar_first']); ?></aside>
    <?php endif; ?>
    <article>

      <?php print $messages; ?>
      <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?> 
      <?php if($is_recipe){ print '<div ' . $schema_itemscope . '>'; }?>     	
  		  <?php if(arg(0) != 'user'){ ?>
          <h1 class="title" id="page-title"><span itemprop="name"><?php print $title; ?></span></h1>
        <?php } ?>
  		  <?php print render($page['help']); ?>
  		  <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
      	<?php print render($page['content']); ?>
      <?php if($is_recipe){ print '</div>'; } ?>
	</article>		
</div>
<footer><?php print render($page['footer']); ?></footer>

<div id="copyright">© 2016-<?php echo date('Y'); ?> cooklikemary.ru Все права защищены При использовании материалов сайта, ссылка на источник обязательна</div>