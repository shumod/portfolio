<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php $row_num = 0; 
	foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php 
    	$row_num++;
    	if($row_num < 3){
    		print $row;
    	}
    	else{
    		$author = l('Мария, автор', 'user/10');
    		print '<div class="blog-author"><div class="blog-author-image"><img src="/sites/all/themes/cooklikemary/images/maria.jpg" /></div>';
    		print '<div class="blog-author-name">'.$author.'</div></div>';

    	} 
    ?>
  </div>
<?php endforeach; ?>