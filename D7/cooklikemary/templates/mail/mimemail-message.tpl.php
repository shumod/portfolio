<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
if($module != 'simplenews'){
  $class = 'not-simplenews';
  $header = '
    <div class="mimemail-header">  
      <table>
        <tr>
          <td class="hello-friends" width="85%"><!--<img src="http://cooklikemary.ru/sites/all/themes/cooklikemary/images/slogan-all.jpg" />--></td>
            <td class="logo" width="15%"><img src="http://cooklikemary.ru/sites/all/themes/cooklikemary/images/logo.jpg" /></td>
        </tr>
      </table>
    </div>';
  $footer = '
    <p style="margin: 0px; margin-right: 30px; margin-left: 30px;">
      <img src="http://cooklikemary.ru/sites/all/themes/cooklikemary/images/your-mary.png" style="width: auto;" />
    </p>
  ';
}
else{
  $class = '';
  $header = '';
  $footer = '';
}

?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
    <?php endif; ?>
  </head>
  <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .' mimemail-'.$module.' '.$class.'"'; endif; ?>>
    <div id="center">
      <div id="main">
        <?php print $header ?>
        <div class="mimemail-body-wrapper"><?php print $body ?></div>
        <?php print $footer ?>
      </div>
    </div>
  </body>
</html>
