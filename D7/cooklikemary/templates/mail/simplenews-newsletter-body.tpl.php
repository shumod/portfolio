<?php

/**
 * @file
 * Default theme implementation to format the simplenews newsletter body.
 *
 * Copy this file in your theme directory to create a custom themed body.
 * Rename it to override it. Available templates:
 *   simplenews-newsletter-body--[tid].tpl.php
 *   simplenews-newsletter-body--[view mode].tpl.php
 *   simplenews-newsletter-body--[tid]--[view mode].tpl.php
 * See README.txt for more details.
 *
 * Available variables:
 * - $build: Array as expected by render()
 * - $build['#node']: The $node object
 * - $title: Node title
 * - $language: Language code
 * - $view_mode: Active view mode
 * - $simplenews_theme: Contains the path to the configured mail theme.
 * - $simplenews_subscriber: The subscriber for which the newsletter is built.
 *   Note that depending on the used caching strategy, the generated body might
 *   be used for multiple subscribers. If you created personalized newsletters
 *   and can't use tokens for that, make sure to disable caching or write a
 *   custom caching strategy implemention.
 *
 * @see template_preprocess_simplenews_newsletter_body()
 */
$node = $build['#node'];
?>
<div class="body-bg">
	<div class="body-wrapper">
		<div class="body-header">
			<table>
			  <tr>
			    <td class="hello-friends" width="60%"><img src="http://cooklikemary.ru/sites/all/themes/cooklikemary/images/slogan.jpg" /></td>
			    <td class="logo" width="40%" valign="top"><img src="http://cooklikemary.ru/sites/all/themes/cooklikemary/images/logo.jpg" /></td>
			  </tr>
			</table>
		</div>
		<h2><?php print $title; ?></h2>
		<div class="body" style="background: url(http://cooklikemary.ru/sites/all/themes/cooklikemary/images/right-bg.png) 95% 100% no-repeat;">
		   <?php 
		     if($node->type == 'simplenews'){
		     	if(isset($node->field_slider_image[LANGUAGE_NONE][0]['uri'])){
			     	$image = image_style_url('width_1000_mail', $node->field_slider_image[LANGUAGE_NONE][0]['uri']);
			     	$arr = explode('?', $image);
			     	echo '<img src="'.$arr[0].'" />';	
		     	}
					echo '<div id="node-body">'.$node->body[LANGUAGE_NONE][0]['value'].'</div>';	     	
		     }
		     else{
		       print render($build); 
		     }
		   ?>
		   <p style="margin: 0px; margin-right: 30px; margin-left: 30px;">
			<img src="http://cooklikemary.ru/sites/all/themes/cooklikemary/images/your-mary.png" style="width: auto;" />
	        </p>
		</div>
	</div>
</div>