<div id="container">
	<header>
  	<?php if ($logo): ?>
  		<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
    	<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
  	<?php endif; ?>
	  <span id="slogan"><?php print $site_slogan; ?></span>
    <div id="mobile-menu-button">Моб меню кнопка</div>
    <div id="mobile-menu">
      <div id="mobile-menu-close">Моб меню закрыть</div>
      <?php print render($page['header']); ?>
     	<nav id="main-menu" class="navigation">
				<?php print theme('links__system_main_menu', array(
          		'links' => $main_menu,
          		'attributes' => array(
           		'id' => 'main-menu-links',
            	'class' => array('links', 'clearfix'),
          		),
				)); ?>
			</nav> <!-- /#main-menu -->
    </div>
	</header>
    <article>
        <?php print $messages; ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>      	
		<?php print render($page['help']); ?>
		<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
      	<?php print render($page['content']); ?>
	</article>		
</div>
<footer><?php print render($page['footer']); ?></footer>
<div id="copyright">© 2016-<?php echo date('Y'); ?> cooklikemary.ru Все права защищены При использовании материалов сайта, ссылка на источник обязательна</div>