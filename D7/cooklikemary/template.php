<?php

/**
 * Preprocess function for page.tpl.php.
 */
function cooklikemary_preprocess_html(&$vars) {
  drupal_add_library('system', 'jquery.cookie');

  // Добавляем скрипты на главную
  if(drupal_is_front_page()){
    drupal_add_js(path_to_theme() . '/js/cooklikemary_front_page.js');
  }

  // Убираем класс sidebar-first из тэга body, чтобы на странице рецепта не было левой колонки
  if(arg(0) == 'node' && is_numeric(arg(1))){
    $node = node_load(arg(1));
    if($node->type == 'recipe'){
      foreach ($vars['classes_array'] as $key => $class) {
        if (strpos($class, 'sidebar-first') !== false) {
          unset($vars['classes_array'][$key]);
        }
      }
    }
  }
  
  // Выводим социальные кнопки
  $social = _return_social_links();
  $vars['social_links_script'] = $social['social_links_script'];
  $vars['social_links'] = $social['social_links'];
}

/**
 * Preprocess function for page.tpl.php.
 * Убираем левую колонку на странице рецепта
 */
function cooklikemary_preprocess_page(&$vars, $hook) {
  $vars['is_recipe'] = FALSE;
  if(isset($vars['node'])){
    if($vars['node']->type == 'recipe'){
      unset($vars['page']['sidebar_first']);
      $vars['is_recipe'] = TRUE;
      $vars['schema_itemscope'] = ' itemscope itemtype="http://schema.org/Recipe" ';
    }
  }

  // Меняем заголовок на странице логина
  global $user;
  if($user->uid == 0 && arg(0) == 'user' && arg(1) == ''){
    drupal_set_title('Вход на сайт');
  }

  if(arg(0) == 'print'){
    drupal_set_message('Сообщение на зеленом фоне');
    drupal_set_message('Сообщение на красном фоне', 'error');
  }
}

/**
 * Preprocess function for node.tpl.php.
 */
function cooklikemary_preprocess_node(&$vars) {

  if($vars['type'] == 'recipe'){
    drupal_add_css(path_to_theme() . '/print.css');
  }

  //Настраиваем темплейт для тизеров. Теперь тизер можно темизировать через файл node--type--view_mode.tpl.php
  $node_type_suggestion_key = array_search('node__' . $vars['type'], $vars['theme_hook_suggestions']);
  if ($node_type_suggestion_key !== FALSE) {
    $node_view_mode_suggestion = 'node__' . $vars['type'] . '__' . $vars['view_mode'];
    array_splice($vars['theme_hook_suggestions'], $node_type_suggestion_key + 1, 0, array($node_view_mode_suggestion));
  }
  
  // Маленькая картинка
  $custom_default_image_path = 'public://default_avatar.png';
  $user = user_load($vars['node']->uid);
  $user_picture_min = theme(
  	'image_style', 
  	array('style_name' => 'round_45x45', 
  		'path' => !empty($user->picture->uri) ? $user->picture->uri : $custom_default_image_path));
  $options = array('html' => TRUE);
  
  // Если это Мария, то добавляем класс к аватарке
  if($user->uid == 10){
    $options['attributes']['class'][] = 'avatar-mary';
  }
  $vars['user_picture_min'] = l($user_picture_min, 'user/' . $user->uid, $options);

  // Имя автора
  if(isset($user->field_user_name[LANGUAGE_NONE][0]['value'])){
    $user_name = $user->field_user_name[LANGUAGE_NONE][0]['value'];
  }
  else{
    $user_name = $user->name;
  }
  $vars['user_name'] = l($user_name, 'user/' . $user->uid);

  // Количество лайков рецепта
  $flag_count = flag_get_counts('node', $vars['nid']);
  if(isset($flag_count['favorites'])){
    $vars['likes_count'] = $flag_count['favorites'];
  }
  else{
    $vars['likes_count'] = 0;
  }

  
  if($vars['type'] == 'recipe' && $vars['view_mode'] == 'full'){
    drupal_add_js(path_to_theme() . '/js/cooklikemary_recipe.js');

    // Выводим большую картинку в самом начале
    $element = array();
    $items = array();

    if(isset($vars['field_recipe_main_image'][0])){
      foreach ($vars['field_recipe_main_image'] as $key => $image) {
        $alt = isset($image['alt']) ? $image['alt'] : '';
        $title = isset($image['title']) ? $image['title'] : '';
        $image_style = theme(
          'image_style', 
          array('style_name' => 'top_slider', 
          'path' => $image['uri'],
          'alt' => $alt,
          'title' => $title
          )
        );
        $big_image_url = file_create_url($image['uri']);
        $link_attr = array(
          'html' => TRUE,
          'attributes' => array(
            'rel' => array('colorbox-gallery'),
            'class' => array('colorbox'),
            //'itemprop' => array('resultPhoto'),
          )
        );
        $items[] = array(
          'slide'   => l($image_style, $big_image_url, $link_attr),
        );
      }
    }

    // Add options.
    $options = array(
      'autoplay' => TRUE,
      'arrows'   => TRUE,
    );

    // Build the slick.
    $gallery = slick_build($items, $options);
    $vars['top_gallery'] = render($gallery);

    // Выводим список ингредиентов
    $ingredient_array = array();
    if(isset($vars['field_recipe_ingredient'])){
      foreach ($vars['field_recipe_ingredient'] as $key => $field_collection_item) {
        $ingredient_array[] = $field_collection_item['value'];
      }
    }
    $ingredients = entity_load('field_collection_item', $ingredient_array);

    $header = array();
    $rows = array();
    $vars['ingredients_table'] = '';

    $ingredients_array = array();
    foreach ($ingredients as $key => $ingredient) {

      //Формируем строки таблицы
      $input = '<input type="checkbox" id="ingredient-'.$key.'" class="ingredient-checkbox" data-ingredient="'.$key.'">' .
        '<label for="ingredient-'.$key.'"></label>';
      if(isset($ingredient->field_recipe_ingredient_name[LANGUAGE_NONE][0]['taxonomy_term']->name)){
        if($ingredient->field_recipe_ingredient_is_link[LANGUAGE_NONE][0]['value']){
          $name = $ingredient->field_recipe_ingredient_name[LANGUAGE_NONE][0]['taxonomy_term']->name;
          $tid = $ingredient->field_recipe_ingredient_name[LANGUAGE_NONE][0]['taxonomy_term']->tid;
          $ingredient_name = '<span itemprop="recipeIngredient">' . 
          l($name, 'taxonomy/term/' . $tid) .
          '</span>';
        }
        else{
          $ingredient_name = '<span itemprop="recipeIngredient">' . $ingredient->field_recipe_ingredient_name[LANGUAGE_NONE][0]['taxonomy_term']->name .'</span>';
        }
      }
      else{
        $ingredient_name = '';
      }

      if(isset($ingredient->field_recipe_ingredient_count[LANGUAGE_NONE][0]['value'])){
        $ingredient_count = '<span>' . $ingredient->field_recipe_ingredient_count[LANGUAGE_NONE][0]['value'] .'</span>';
      }
      else{
        $ingredient_count = '';
      }

      // Формируем таблицу для вывода ингредиентов и их групп
      if(isset($ingredient->field_recipe_ingredient_group[LANGUAGE_NONE][0])){
        $ingredients_array[$ingredient->field_recipe_ingredient_group[LANGUAGE_NONE][0]['tid']][$key] = array($input, $ingredient_name, $ingredient_count);
      }
      else{
        $ingredients_array['no-group'][$key] = array($input, $ingredient_name, $ingredient_count);
      }
    }

    if(isset($vars['field_recipe_addition_ingredient'][0]['value'])){
      $input = '<input type="checkbox" id="ingredient-additional" class="ingredient-checkbox" data-ingredient="additional">' .
  '<label for="ingredient-additional"></label>';
      $ingredient_name = $vars['field_recipe_addition_ingredient'][0]['value'];
      $ingredients_array['additional'][] = array($input, $ingredient_name);
    }

    // Перемещаем элементы без группы вверх
    if(isset($ingredients_array['no-group'])){
      $no_group = array('no-group' => $ingredients_array['no-group']);
      unset($ingredients_array['no-group']);
      $ingredients_array = $no_group + $ingredients_array;
    }

    // Выводим ингредиенты в таблице
    foreach ($ingredients_array as $group => $ingredients_value) {
      if($group != 'no-group' && $group != 'additional'){
        $term = taxonomy_term_load($group);
        $rows[] = array(
          'data' => array(
            array(
              'data' => '<span>'.$term->name.'</span>', 
              'colspan' => 3, 
              'class' => array('group-name')
            )
          ),
          'class' => array('ingredient-group-' . $group),
        );         
      }

      if($group == 'additional'){
        $rows[] = array(
          'data' => array(
            array(
              'data' => $ingredients_value[0][0], 
            ),
            array(
              'data' => '<span>' . $ingredients_value[0][1] . '</span>',
              'colspan' => 2, 
              'class' => array('additional-value')
            )
          ),
          'class' => array('ingredient-row-' . $group),
        );        
      }
      else{
        foreach ($ingredients_value as $ingredient_id => $ingredient_array) {
          $rows[] = array(
            'data' => $ingredient_array,
            'class' => array('ingredient-row-' . $ingredient_id),
          );        
        }
      }
    }

    $vars['ingredients_table'] = theme('table', array('header' => $header, 'rows' => $rows));

    // Выводим тэги рецепта
    $recipe_tags = '';
    $field_vocabularies = array('tax_category', 'tax_kitchen', 'tax_reason', 'tax_tag');
    foreach ($field_vocabularies as $field_part_name) {
      $field_name = 'field_recipe_' . $field_part_name;
      if(isset($vars[$field_name][0]['taxonomy_term']->tid)){
        foreach ($vars[$field_name] as $key => $field_value) {
          $attr = array();
          if($field_value['taxonomy_term']->vid == 5){ // Если это национальная кухня
            $attr = array(
              'attributes' => array(
                'itemprop' => array('recipeCuisine'),
              )
            );
          }
          else if($field_value['taxonomy_term']->vid == 2){
            $attr = array(
              'attributes' => array(
                'itemprop' => array('recipeCategory'),
              )
            );
          }
          $recipe_tags .= l($field_value['taxonomy_term']->name, 'taxonomy/term/' . $field_value['tid'], $attr);
        }
      }
    }

    $vars['recipe_tags'] = $recipe_tags;

    // Выводим шаги рецепта
    $steps_array = array();
    foreach ($vars['field_recipe_steps'] as $key => $field_collection_item) {
      $steps_array[] = $field_collection_item['value'];
    }
    $steps = entity_load('field_collection_item', $steps_array);

    $vars['recipe_steps'] = array();
    foreach ($steps as $key => $step) {
      $step_image = '';
      // Выводим картинки и класс для них, если нужно вывести 2 картинки
      if(isset($step->field_recipe_step_image[LANGUAGE_NONE][0]['uri'])){

        foreach ($step->field_recipe_step_image[LANGUAGE_NONE] as $key => $simage) {
          $step_image .= theme(
            'image_style', 
            array(
            'style_name' => 'width_700', 
            'path' => $simage['uri'],
            'alt' => isset($simage['alt']) ? $simage['alt'] : '',
            'title' => isset($simage['title']) ? $simage['title'] : '',
            'attributes'=> array('itemprop' => array('image'), 'class' => 'step-image')));
        }

      }

      if(isset($step->field_recipe_step_description[LANGUAGE_NONE][0]['value'])){
        $step_text = $step->field_recipe_step_description[LANGUAGE_NONE][0]['value'];
      }
      else{
        $step_text = '';
      }

      $vars['recipe_steps'][] = array('image' => $step_image, 'text' => $step_text);
    }

    $vars['count_to'] = 0;  
    if(isset($vars['field_recipe_persons_count_to'][0])){
      $vars['count_to'] = 1;
    }
  }

  // Переменная для вывода флага избранного
  global $user;
  if($user->uid != 0){
    $flag_link = flag_create_link('favorites', $vars['nid']);
  }
  else{
    $flag_link = '<span class="flag-wrapper"><a href="/node/39" class="autodialog flag flag-action" data-dialog-height="400">Зарегистрироваться</a></span>';
  }
  $vars['flag_link'] = $flag_link;

  // Выводим социальные кнопки
  $social = _return_social_links();
  $vars['social_links_script'] = $social['social_links_script'];
  $vars['social_links'] = $social['social_links'];

  //$button = printfriendly_create_button('', true);
  //$print_button = '<div class="print-button"></div>';
  $vars['print_button'] = 
    '<div class="print-button" onclick="window.print()">
      <img src="/sites/all/themes/cooklikemary/images/print_verision.png" />
    </div>';


  // Социальные комментарии
  $social_comments = '
    <div style="margin: 0px auto; text-align: center;" class="social-comments">
      <br><h2 class="title comment-form">Комментировать через ВКонтакте:</h2>
        <!-- Put this script tag to the <head> of your page -->
      <!-- Put this script tag to the <head> of your page -->
      <script type="text/javascript" src="//vk.com/js/api/openapi.js?152"></script>

      <script type="text/javascript">
        VK.init({apiId: 6391901, onlyWidgets: true});
      </script>

      <!-- Put this div tag to the place, where the Comments block will be -->
      <div id="vk_comments" style="margin: 0px auto;"></div>
      <script type="text/javascript">
      VK.Widgets.Comments("vk_comments", {limit: 10, width: "550", attach: "*"});
      </script>

      <h2 class="title comment-form">Комментировать через Facebook:</h2>
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.12&appId=108428795920208&autoLogAppEvents=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, "script", "facebook-jssdk"));</script>

      <div class="fb-comments" data-href="https://cooklikemary.ru/" data-width="600" data-numposts="5"></div>
    </div>
  ';

  $vars['social_comments'] = $social_comments;


}

/**
 * Preprocess function for comment.tpl.php.
 */
function cooklikemary_preprocess_comment(&$vars) {

  // Маленькая картинка
  $custom_default_image_path = 'public://default_avatar.png';
  $user = user_load($vars['comment']->uid);
  $user_picture_min = theme(
    'image_style', 
    array('style_name' => 'round_66x66', 
      'path' => !empty($user->picture->uri) ? $user->picture->uri : $custom_default_image_path));
  $vars['user_picture_min'] = l($user_picture_min, 'user/' . $user->uid, array('html' => TRUE));

  // Выводим дату публикации коммента
  $vars['comment_date'] = date('d.m.Y', $vars['comment']->created);

  // Имя автора
  if(isset($user->field_user_name[LANGUAGE_NONE][0]['value'])){
    $user_name = $user->field_user_name[LANGUAGE_NONE][0]['value'];
  }
  else{
    $user_name = $user->name;
  }
  $vars['user_name'] = l($user_name, 'user/' . $user->uid);
}

/*
 * Возвращает количество нод пользователя
 */
function cooklikemary_get_user_recipes_count($uid) {
  $query = db_select('node', 'n');
  $query->condition('type', 'recipe', '=');
  $query->condition('uid', $uid, '=');
  $query->condition('status', '1', '=');
  $query->addExpression('COUNT(1)', 'count');
  $result = $query->execute();
 
  if ($record = $result->fetchAssoc()) return $record['count'];
   
  return 0;
}

/**
 * Preprocess function for search-result.tpl.php.
 * Правим имя пользователя в результатах поиска
 */
function cooklikemary_preprocess_search_result(&$variables) {

  // Имя автора
  $result = $variables['result'];
  $user = $variables['user'];

  $user = user_load($variables['result']['node']->uid);
  if(isset($user->field_user_name[LANGUAGE_NONE][0]['value'])){
    $user_name = $user->field_user_name[LANGUAGE_NONE][0]['value'];
  }
  else{
    $user_name = $user->name;
  }

  $info = array();
  if (!empty($result['module'])) {
    $info['module'] = check_plain($result['module']);
  }
  if (!empty($result['user'])) {
    $info['user'] = l($user_name, 'user/' . $user->uid);
  }
  if (!empty($result['date'])) {
    $info['date'] = format_date($result['date'], 'short');
  }
  if (isset($result['extra']) && is_array($result['extra'])) {
    $info = array_merge($info, $result['extra']);
  }
  $variables['info_split'] = $info;
  $variables['info'] = implode(' - ', $info);
}

function _return_social_links(){
  $output = array();
  // Выводим социальные кнопки
  $output['social_links_script'] = "<script type=\"text/javascript\">(function(w,doc) {
  if (!w.__utlWdgt ) {
      w.__utlWdgt = true;
      var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
      s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
      s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
      var h=d[g]('body')[0];
      h.appendChild(s);
  }})(window,document);
  </script>";
  $output['social_links'] = '<div data-background-alpha="0.0" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round" data-sn-ids="fb.vk.tw.ok.gp.ps." data-share-size="30" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.ok.ps.gp." data-pid="1592465" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="false" data-selection-enable="true" class="uptolike-buttons"></div>';
  return $output;
}

function cooklikemary_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('«')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('»')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}