# Портфолио Бородина Александр
Модули и темы выполненные для Drupal 7 и 8 + Javascript с использованием ООП без фреймворков.


## Drupal 8
#### [prosvetcult](https://bitbucket.org/shumod/portfolio/src/master/D8/prosvetcult/)
Модули, управляющие наполнением сайта [Культурный навигатор](http://prosvetcult.ru): 

[pro_api](https://bitbucket.org/shumod/portfolio/src/master/D8/prosvetcult/pro_api) - API для взаимодействия с https://all.culture.ru/public/json/howitworks

[pro_content](https://bitbucket.org/shumod/portfolio/src/master/D8/prosvetcult/pro_content) - отвечает за вывод контента(событий, мест и статей)

[pro_front](https://bitbucket.org/shumod/portfolio/src/master/D8/prosvetcult/pro_front) - работа главной страницы

[pro_regions](https://bitbucket.org/shumod/portfolio/src/master/D8/prosvetcult/pro_regions) - работа с регионами(загрузка, выгрузка, вывод) 

#### [cpayment](https://bitbucket.org/shumod/portfolio/src/master/D8/cpayment/) + [cpayment_entity](https://bitbucket.org/shumod/portfolio/src/master/D8/cpayment_entity/)
Модули для оплаты через Robokassa игр и билетов на сайте http://psiho.games/

## Drupal 7
#### [np_adv](https://bitbucket.org/shumod/portfolio/src/master/D7/np_adv/)
Модуль для добавления, удаления рекламных блоков и токенов 
для вставки рекламы в контент сайта.
#### [cooklikemary](https://bitbucket.org/shumod/portfolio/src/master/D7/cooklikemary/)
Тема для сайта http://cooklikemary.ru/

## Javascript
#### Игра [Lila](https://bitbucket.org/shumod/portfolio/src/master/Javascript/lila/)
Игра Лила чакра для сайта http://psiho.games/. Написана на Javascript ES6 c использованием
node_js, websockets, babel.