'use strict';
import Game from "./Common/Game";

window.game = new Game();

document.addEventListener('DOMContentLoaded', () => {
  game.start();
});