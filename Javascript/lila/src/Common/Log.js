'use strict';
import {player} from "./Player";
import {strings} from "./Strings";
import Level from "./Level";
import {services} from "./ServiceFunctions";

import forEach from 'lodash/forEach';
import template from 'lodash/template';
import size from 'lodash/size';
import escape from 'lodash/escape';

import $ from 'jquery';

export default class Log{
  constructor(logData){
    this.player = player;
    this.data = logData;
    this.levelsMap = this.getLevelsMap();
    this.chakrasMap = this.getChakrasMap();
  }

  /**
   * Отдаёт сам лог
   * @returns {*}
   */
  getData(){
    return this.data;
  }

  /**
   * Отдаёт номер последнего хода
   * @returns {Number}
   */
  getLastTurn(){
    return Object.keys(this.data).length
  }

  /**
   * Сколько было сделано 2-х ходов
   * @returns {*}
   */
  getTurn2MoveSize(turnNum){
    turnNum = services.isNumeric(turnNum) ? turnNum : this.getLastTurn();

    if(this.data[turnNum].secondMove){
      return Object.keys(this.data[turnNum].secondMove).length;
    }

    return 0;
  }

  /**
   * Отдаёт текст последнего броска
   * @returns {string}
   */
  getLastRollText(){
    let lastText = '',
        lastTurn = this.getLastTurn(),
        firstMove = this.data[lastTurn].firstMove;

    /**
     * Если был кинут кубик для второго хода.
     */
    if(this.getTurn2MoveSize() === 1){
      if(firstMove.rollText){ //Если есть сохранённый текст, то выводим его
        lastText = firstMove.rollText;
      }
      else{ //Если нет сохранённого текста, тогда выводим дефолтную строку
        let level1 = new Level(firstMove.levelNum),
            roll = firstMove.result,
            moveText = strings.data.youMoveTo;

        let moveTextReplace = moveText.replace('@result', roll).replace('@levelNum', level1.getNumAndName());

        lastText = moveTextReplace;
      }
    }

    return lastText;
  }

  /*
   * Сохраняем данные в лог игры.
   */

  /**
   * Сохраняем данные для последнего второго хода
   * @param data
   */
  setLast1MoveData(moveData){
    let lastTurn = this.getLastTurn();

    forEach(moveData, (value, key) => {
      this.data[lastTurn].firstMove[key] = value;
    });
  }

  /**
   * Сохраняем данные для последнего второго хода
   * @param data
   */
  setLast2MoveData(moveData){
    let lastTurn = this.getLastTurn(),
        lastTurn2MoveSize = this.getTurn2MoveSize();

    if(lastTurn2MoveSize === 0){
      this.data[lastTurn].secondMove = {};
      lastTurn2MoveSize = 1;
    }

    if(!this.data[lastTurn].secondMove[lastTurn2MoveSize]){
      this.data[lastTurn].secondMove[lastTurn2MoveSize] = {};
    }

    forEach(moveData, (value, key) => {
      this.data[lastTurn].secondMove[lastTurn2MoveSize][key] = value;
    });
  }

  /**
   * Сохраняем данные для следующего второго хода
   * @param data
   */
  setNext2MoveData(moveData){
    let lastTurn = this.getLastTurn(),
        lastTurn2MoveSize = +this.getTurn2MoveSize() + 1;

    if(!this.data[lastTurn].secondMove[lastTurn2MoveSize]){
      this.data[lastTurn].secondMove[lastTurn2MoveSize] = {};
    }

    forEach(moveData, (value, key) => {
      this.data[lastTurn].secondMove[lastTurn2MoveSize][key] = value;
    });
  }

  /**
   * Сохраняем данные в лог после броска кубика 1 хода
   * @param result
   */
  setData1MoveAfterRollDice(result){
    let lastTurn = this.getLastTurn(),
        lastTurn2MoveSize = this.getTurn2MoveSize();

    this.data[lastTurn].firstMove.result = result;

    if(this.player.pain){ //Сохраняем боли, если они есть
      this.data[lastTurn].firstMove.pain = this.player.pain;
    }

    this.data[lastTurn].secondMove = {
      [lastTurn2MoveSize + 1]: {      // +1 чтобы начинать с 1, а не с нуля
        levelNum: this.player.levelNum
      }
    }
  }

  /**
   * Сохраняем данные, когда стоим после броска 2 хода
   * @param result
   */
  setData2StayAfterRollDice(result){
    let lastTurn = this.getLastTurn(),
        lastTurn2MoveSize = this.getTurn2MoveSize();

    this.data[lastTurn].secondMove[lastTurn2MoveSize].result = result;
  }

  /**
   * Сохраняем результат броска кубика и новый уровень игрока в лог.
   * @param result
   * @param levelNum
   */
  setData2MoveAfterRollDice(result, levelNum){
    let lastTurn = this.getLastTurn(),
      lastTurn2MoveSize = this.getTurn2MoveSize();

    this.data[lastTurn].secondMove[lastTurn2MoveSize].result = result;
    this.data[lastTurn].secondMove[lastTurn2MoveSize + 1] = {
      levelNum
    }
  }

  /**
   * Установить выбранную плеером позицию для осознания.
   * @param knowingNum
   */
  setKnowing(knowingNum){
    let lastTurn = this.getLastTurn();
    this.data[lastTurn].knowing = +knowingNum;
  }

  /**
   * Отдаёт номер позиции для осознания для хода
   * @param turnNum
   */
  getKnowingNum(turnNum){
    turnNum = services.isNumeric(turnNum) ? turnNum : this.getLastTurn();
    return this.data[turnNum].knowing || 0;
  }

  /**
   * Получаем текст позиции для осознания для определённого хода
   * @param turnNum
   * @returns {*}
   */
  getKnowingText(turnNum, levelNum){
    let knowingNum = this.getKnowingNum(turnNum);

    levelNum = services.isNumeric(levelNum) ? levelNum : player.levelNum;

    return game.data.json.levels.levelMap[levelNum].knowing[knowingNum] || '';
  }

  /**
   * Удаляем позицию для осознания
   */
  removeLastKnowing(){
    let turnNum = this.getLastTurn();
    delete this.data[turnNum].knowing;
  }

  /**
   * Начинаем новый ход
   */
  startNewTurn(){
    let nextTurnNum = this.getLastTurn() + 1;
    this.data[nextTurnNum] = {
      firstMove: {
        levelNum: player.levelNum
      }
    }
  }

  /**
   * Печатаем лог
   * @param selector
   */
  print(selector){
    let data = this.prepareData(),
        logHtml = template(game.data.template.log)({data, forEach});

    selector = selector ? selector : '.game-log';

    $(selector).html(logHtml);
  }

  /**
   * Готовим данные, чтобы отдать их в лог
   * @returns {{}}
   */
  prepareData(){
    let data = this.data,
        output = {};

    forEach(data, (log, turnNum) => {
      let startMoveText = this.startMoveText(data, log, turnNum),
          secondMoveText = '',
          lastMoveText = '',
          knowingText = '',
          comment = '';

      if(log.secondMove){
        secondMoveText = this.secondMoveText(data, log, turnNum);
      }

      if(log.knowing){
        lastMoveText = this.lastMoveText(data, log, turnNum);
        knowingText = this.getKnowingText(turnNum, this.getLastLevelNum(turnNum));
      }

      if(log.comment){
        comment =
          `<strong>Комментарий:</strong> 
            ${log.comment}
            <span onclick="game.player.logClass.editComment(${turnNum})"
                  class="log-comment-link log-comment-edit-link">
            </span>`;
      }
      else{
        comment =
          `<span onclick="game.player.logClass.addComment(${turnNum})" 
                 class="log-comment-link log-comment-add-link">
           Добавить комментарий</span>`;
      }

      output[turnNum] = {
        startMove: startMoveText,
        secondMove: secondMoveText,
        lastMove: lastMoveText,
        knowing: knowingText,
        comment
      };
    });

    return output;
  }

  /**
   * Текст для начала записи лога
   * @param data - весь лог
   * @param log - один ход
   * @param turnNum - номер хода
   */
  startMoveText(data, log, turnNum){
    let output = '';

    if(log.firstMove.result){
      output += `Начало хода: к${log.firstMove.result}`;
    }

    if(log.firstMove.pain){
      let pains = services.pluralize(log.firstMove.pain, 'разочарование', 'разочарования', 'разочарований');
      output += `(-${pains})`;
    }

    if(log.secondMove && log.secondMove[1] && typeof log.secondMove[1].levelNum !== undefined){
      let l = new Level(log.secondMove[1].levelNum);
      output += `, переход на ${l.getNumAndName()}`;
    }

    if(log.firstMove.description){
      let d = log.firstMove.description;
      let matches = d.match(/@[\w]{0,}/ig);

      forEach(matches, (match, matchNum) => {
        match = match.replace('@', '');
        d = Log.getMatchText(d, match, log.firstMove);
      });

      output = d;
    }

    return output;
  }

  /**
   * Текст для вторых ходов
   * @param data - весь лог
   * @param log - один ход
   * @param turnNum - номер хода
   */
  secondMoveText(data, log, turnNum){
    let listArr = [];

    forEach(log.secondMove, (secondMove, secondMoveNum) => {
      secondMoveNum = +secondMoveNum;

      if(typeof secondMove.levelNum !== undefined){
        if(secondMove.description){
          let d = secondMove.description;
          let matches = d.match(/@[\w]{0,}/ig);

          forEach(matches, (match, matchNum) => {
            match = match.replace('@', '');
            d = Log.getMatchText(d, match, secondMove);
          });

          listArr.push(d);
        }
        else{
          if(log.secondMove[secondMoveNum + 1] && log.secondMove[secondMoveNum + 1].levelNum){
            let l = new Level(log.secondMove[secondMoveNum + 1].levelNum);

            listArr.push(
              `к${log.secondMove[secondMoveNum].result}, переход на ${l.getNumAndName()}`
            );
          }
        }
      }
    });

    return this.createListFromArray(listArr);
  }

  /**
   * Заменяет в description совпадения match. При этом берёт данные из move.
   * @param description
   * @param match
   * @param move
   * @returns {*}
   */
  static getMatchText(description, match, move){
    let result = move[match];
    switch (match){
      case 'levelNum':
      case 'newLevelNum':
        let l = new Level(result);
        description = description.replace('@' + match, l.getNumAndName());
        break;

      case 'result':
        let r = services.pluralize(result[0], 'клетку', 'клетки', 'клеток');
        description = description.replace('@' + match, r);
        break;

      case 'newTarget':
        description = description.replace('@' + match, `<blockquote>${result}</blockquote>`);
        break;

      case 'levels':
        let count = services.pluralize(result, 'клетку', 'клетки', 'клеток');
        description = description.replace('@' + match, count);
        break;
    }

    return description;
  }

  /**
   * Текст для окончания хода
   * @param data
   * @param log
   * @param turnNum
   */
  lastMoveText(data, log, turnNum){
    let output = '';
    if(log.secondMove){
      let secondSize = size(log.secondMove),
          l = new Level(log.secondMove[secondSize].levelNum);

      output = `Конец хода: ${l.getNumAndName()}`;
    }

    return output;
  }

  /**
   * Получить последний номер уровня, на котором находится игрок
   * @param turnNum
   */
  getLastLevelNum(turnNum){
    let last = this.getTurn2MoveSize(turnNum);

    if(last > 0){
      if(this.data[turnNum].secondMove[last].levelNum){
        return this.data[turnNum].secondMove[last].levelNum;
      }
      else{
        if(last > 1){
          return this.data[turnNum].secondMove[last - 1].levelNum;
        }
        else{
          return this.data[turnNum].firstMove.levelNum;
        }
      }
    }
    else{
      return this.data[turnNum].firstMove.levelNum;
    }
  }

  /**
   * Отдаёт html список из массива элементов
   * @param arr
   * @returns {string}
   */
  createListFromArray(arr){
    let list = document.createElement('ul');

    for(let i = 0; i < arr.length; i++) {
      let item = document.createElement('li');
      item.innerHTML = arr[i];
      list.appendChild(item);
    }

    return list.outerHTML;
  }


  /**
   * Работа с комментариями лога
   */

  /**
   * Получить текст комментария
   * @param turnNum
   * @returns {string|*|comment}
   */
  getCommentText(turnNum){
    return game.player.logClass.data[turnNum].comment;
  }

  /**
   * Добавляем форму комментария для лога
   * @param turnNum
   */
  addComment(turnNum){
    game.player.logClass.print('.game-log');

    let commentForm = game.player.logClass.getCommentForm(turnNum);
    document.querySelector(`.log-${turnNum} .move-comment`).innerHTML = commentForm;
    document.querySelector('.log-comment-text').focus();
  }

  /**
   * Редактируем комментарий
   * @param turnNum
   */
  editComment(turnNum){
    let text = game.player.logClass.getCommentText(turnNum),
        commentForm = game.player.logClass.getCommentForm(turnNum,text);
    document.querySelector(`.log-${turnNum} .move-comment`).innerHTML = commentForm;
  }

  /**
   * Сохраняем комментарий
   * @param turnNum
   */
  saveComment(turnNum){
    let commentText = document.querySelector(`.log-${turnNum} .log-comment-text`).value;
    if(commentText.trim()){
      this.data[turnNum].comment = escape(commentText);
      game.player.save();
    }
    else{
      alert('Текст комментария не может быть пустым');
    }
  }

  /**
   * Отменяем сохранение комментария
   * @param turnNum
   */
  cancelComment(turnNum){
    game.player.logClass.print('.game-log');
  }

  /**
   * Возвращает форму для комментирования
   * @param turnNum
   */
  getCommentForm(turnNum, text){
    text = text || '';
    return `<textarea class="log-comment-text">${text}</textarea>` +
      `<span class="log-comment-save" onclick="game.player.logClass.saveComment(${turnNum})">Сохранить</span>` +
      `<span class="log-comment-cancel" onclick="game.player.logClass.cancelComment(${turnNum})">Отменить</span>`;
  }

  /**
   * Отдаёт уровни и кол-во их посещений
   * Ключ - номер уровня
   * Значение - кол-во посещений уровня
   */
  getLevelsMap(){
    let map = {};

    forEach(this.data, (turn, turnNum) => {
      if(services.isNumeric(turn.firstMove.levelNum)){
        let fs = turn.firstMove.levelNum;
        map[fs] ? map[fs]++ : map[fs] = 1;
      }

      if(turn.secondMove){
        forEach(turn.secondMove, (secondMove, secondMoveNum) => {
          if(services.isNumeric(secondMove.levelNum) && secondMove.levelNum !== turn.firstMove.levelNum){
            let ls = secondMove.levelNum;
            map[ls] ? map[ls]++ : map[ls] = 1;
          }
        });
      }
    });

    return map;
  }

  /**
   * Получаем карту посещения чакр.
   * Ключ - номер чакры
   * Значение - кол-во посещений
   */
  getChakrasMap(){
    let levelsMap = this.levelsMap,
        chakrasMap = {};

    forEach(levelsMap, (count, levelNum) => {
      let chakraNumber = Math.ceil(levelNum/8);

      if(chakrasMap[chakraNumber]){
        chakrasMap[chakraNumber] += count;
      }
      else{
        chakrasMap[chakraNumber] = count;
      }
    });

    return chakrasMap;
  }

  notifyAboutPlayerSave(){
    this.levelsMap = this.getLevelsMap();
    this.chakrasMap = this.getChakrasMap();
  }
}