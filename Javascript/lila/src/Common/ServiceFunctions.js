'use strict';

import Level from "./Level";

import replace from 'lodash/replace';
import template from 'lodash/template';
import debounce from 'lodash/debounce';
import snakeCase from 'lodash/snakeCase';
import startsWith from 'lodash/startsWith';
import size from 'lodash/size';
import forEach from 'lodash/forEach';


class ServiceFunctions{
  showMessage(data){
    let tplName = replace(data.template, /[0-9]/g, '') || 'message',
        className = replace(snakeCase(tplName), /_/g, '-'),
        tpl = game.data.template[data.template] || game.data.template.message,
        inlineColor = data.level.getColorInlineStyle() || '';

    data.replace = replace;
    data.startsWith = startsWith;
    data.size = size;
    data.forEach = forEach;

    let html = template(tpl)(data);

    $.magnificPopup.open({
      items: {
        src:
          `<div class="white-popup ${className}"><div class="wrapper" ${inlineColor}>${html}</div></div>`,
        type: 'inline'
      }
    });
  }

  /**
   * Отображаем просто сообщение
   * @param data
   */
  showCleanMessage(data){
    let tpl = game.data.template.message,
        html = template(tpl)(data),
        inlineColor = data.inlineColor || '';

    $.magnificPopup.open({
      items: {
        src:
          `<div class="white-popup card-side"><div class="wrapper" ${inlineColor}>${html}</div></div>`,
        type: 'inline'
      }
    });
  }

  /**
   * Показывает описание уровня
   * @param levelNum
   */
  showLevelDescription(levelNum){
    if(this.isNumeric(levelNum)){
      let level = new Level(levelNum);
      let messageAfterHeader =
          level.getDescription() +
          `${level.getNextTurnList()}` +
          `<h3>Позиции для осознания</h3>` +
          `${level.getKnowingsList()}`,
          buttons = [];

      if(game.state.getName() !== 'end'){
        buttons = [{
          func: 'game.state.setActive()',
          value: 'Вернуться к ходу'
        }];
      }

      this.showCleanMessage({
        inlineColor: level.getColorInlineStyle(),
        messageBeforeHeader: '',
        header: level.getNumAndName(),
        messageAfterHeader,
        buttons
      });
    }
  }

  closeMessage(){
    $.magnificPopup.close();
  }

  showPopup(){
    $('.mfp-bg, .mfp-wrap').fadeIn(300);
    $('.show-popup').fadeOut(300);
  }

  hidePopup(){
    if(game.state.getName() !== 'end'){
      $('.show-popup').fadeIn(300);
    }
    $.magnificPopup.close();
/*    $('.mfp-bg, .mfp-wrap').fadeOut(300);
    $('.show-popup').fadeIn(300);
    this.style = $('html').attr('style');
    $('html').attr('style', '');*/

  }

  animate(toSelector, timeout){
    $('html, body').animate({ scrollTop: $(toSelector).offset().top-20 }, timeout);
  }

  animatePlayerMove(toLevelNum, timeout){
    let $level = $(`#level-num-${toLevelNum}`);

    if($level[0]){
      $('html, body').animate({
        scrollTop: $level.offset().top + ($level.height() / 2) - ($(window).height() / 2)
      }, timeout);
    }
  }

  getDisableAlert(){
    alert('Сейчас ходят другие игроки. Дождись своего хода.');
  }

  /**
   * Переводит hex в RGB
   * @param hex
   * @param alpha
   * @returns {string}
   */
  hexToRGB(hex, alpha) {

    let sharpHex = '#' + hex,
        r = parseInt(sharpHex.slice(1, 3), 16),
        g = parseInt(sharpHex.slice(3, 5), 16),
        b = parseInt(sharpHex.slice(5, 7), 16);

    if (alpha) {
      return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
      return "rgb(" + r + ", " + g + ", " + b + ")";
    }
  }

  pluralize(n, one, few, many, addN = true){
    let d = parseInt(n.toString().substr(-1));
    let output = addN ? n + ' ' : ' ';

    if(n > 10 && n < 20) {
      output += many;
    }
    else if(d === 1){
      output += one;
    }
    else if(d >= 2 && d <= 4){
      output += few;
    }
    else {
      output += many;
    }
    return output;
  }

  isNumeric(n){
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
}

export const services = new ServiceFunctions();
window.services = services;


document.addEventListener('DOMContentLoaded', function(){
  $(window).resize(debounce(() => {
    game.movePlayersToTheirLevels();
    game.player.moveToLevelNum(game.player.levelNum, true);
  }, 500));
});