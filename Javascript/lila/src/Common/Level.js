'use strict';
import {player} from "./Player";
import {services} from "./ServiceFunctions";

import forEach from 'lodash/forEach';
import replace from 'lodash/replace';
import size from 'lodash/size';

export default class Level{
  constructor(levelNum){
    this.levelNum =
      services.isNumeric(levelNum) ?
        levelNum :
        services.isNumeric(player.levelNum) ?
          player.levelNum : 0;

    this.levelMap = game.data.json.levels.levelMap[this.levelNum];
  }

  getLevelCoords(){
    let level = document.querySelector('#level-num-' + this.levelNum);
    level = level.getBoundingClientRect();

    return {
      left: level.left + window.scrollX,
      top: level.top + window.scrollY,
      width: level.width,
      height: level.height
    }
  }

  getDescription(){
    return '<p class="level-description">' + this.levelMap.description + '</p>';
  }

  getName(){
    return this.levelMap.name;
  }

  getNumAndName(levelNum){
    let isLink = true,
        logMap = game.player.logClass.levelsMap;

    if(services.isNumeric(levelNum)){
      let newLevel = new Level(levelNum);
      return newLevel.getNumAndName();
    }

    if(!logMap[this.levelNum]){
      isLink = false;
    }

    if(this.levelNum >= 0){
      let name = this.getName().toUpperCase().replace(/&SHY/g, '&shy');
      if(isLink){
        return `<span onclick="services.showLevelDescription(${this.levelNum})" class="cursor-pointer">№${this.levelNum} ${name}</span>`;
      }
      else{
        return `<span>№${this.levelNum} ${name}</span>`;
      }
    }

    return '';
  }

  getColor(){
    return this.levelMap.color;
  }

  getColorInlineStyle(){
    let color = this.getColor().field,
        rgba = services.hexToRGB(color, 0.3);

    return `style="background-color: ${rgba}"`;
  }

  getKnowing(){
    return this.levelMap.knowing;
  }

  getKnowingsList(){
    let knowings = this.getKnowing(),
        output = '';

    forEach(knowings, (knowing, key) => {
      output += `<p>${key}. ${knowing}</p>`;
    });

    return output;
  }

  getNextTurnList(){
    let nextTurns = this.getNextTurn(),
        output = '';

    if(size(nextTurns) > 0){
      output = `<h3>Переходы после броска кубика</h3>`;

      forEach(nextTurns, (nextTurn, key) => {
        let gotoLevel = new Level(nextTurn.levelNum),
          gotoText = replace(nextTurn.description, '@levelNum', gotoLevel.getNumAndName());
        output += `<p>${key} - ${gotoText}</p>`;
      });
    }

    return output;
  }

  getSpecial(type){
    if(this.levelMap.special && this.levelMap.special[type]){
      return this.levelMap.special[type];
    }
    else{
      return '';
    }
  }

  getNextTurn(){
    return this.levelMap.nextTurn;
  }
}