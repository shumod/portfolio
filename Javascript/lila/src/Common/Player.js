'use strict';

import Observable from './Observable.js';
import Log from "./Log";
import Level from "./Level";
import {services} from "./ServiceFunctions";
import axios from 'axios';

import forOwn from 'lodash/forOwn';

class Player extends Observable{
  constructor(){
    super();

    this.setEnable();
    this.timeout = 1000;
  }

  createPlayer(playerObject){
    let _this = this;
    forOwn(playerObject, (value, key) => {
      this[key] = value;
    });

    this.setStateToBody();

    this.image = this.createPlayerImage(playerData.imageSrc);
    this.logClass = new Log(this.log);
    this.attach(this.logClass);
    this.moveToLevelNum(this.levelNum);

    game.player = this;
    game.board.addBaseActionsToBoard();

    this.attach(game);

    this.notify('player created');
  }

  createPlayerImage(src){
    let playerImage = new Image();
    playerImage.src = src;
    playerImage.classList.add('player', 'player-me', 'player-image');

    document.querySelector('body').appendChild(playerImage);

    return playerImage;
  }

  moveToLevelNum(levelNum, isResize = false){
    let moveTimeout = 500;
    services.hidePopup();

    let level = new Level(levelNum),
        levelCoords = level.getLevelCoords();

    let imagePos = this.image.getBoundingClientRect();

    this.image.style.top =
      levelCoords.top + (levelCoords.height / 2) - (imagePos.height / 2) + 'px';
    this.image.style.left =
      levelCoords.left + (levelCoords.width / 2) - (imagePos.width / 2) + 'px';

    services.animatePlayerMove(levelNum, moveTimeout);

    if(!isResize){
      setTimeout(() => {
        this.notifyAboutPlayerStopsMove();
      }, this.timeout);
    }
  }

  save(){
    this.revision++;

    let revision = this.revision || 0,
        savePlayer = {
          name: this.name,
          pain: this.pain,
          target: this.target,
          state: this.state,
          levelNum: this.levelNum,
          log: this.logClass.data,
          test: this.test,
          revision
        };

    localStorage.setItem('player_' + playerData.nid, JSON.stringify(savePlayer));
    this.saveToDB(savePlayer);

    this.logClass.print('.game-log');
    this.setStateToBody();
    this.notifyAboutPlayerSave();
  }

  saveToDB(savePlayer){
    axios.post('/games/save-player?nid=' + playerData.nid , savePlayer)
      .then(function (response) {
        //console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  setOption(name, value){
    this[name] = value;
    this.save();
  }

  /**
   * Смена цели у плеера
   * @param newTarget
   */
  changeTarget(newTarget){
    this.target = newTarget;
    document.querySelector('.game-target').innerHTML = newTarget;
  }

  setEnable(){
    this.enable = 1;
    document.querySelector('.show-popup').style.display = 'block';
    $('body').addClass('player-enabled').removeClass('player-disabled');
    $('.player-me').addClass('active');

    //this.notify('player enable');
  }

  /**
   * Выкл плеера
   */
  setDisable(){
    this.enable = 0;
    $('body').removeClass('player-enabled').addClass('player-disabled');
    $('.player-me').removeClass('active');

    //this.notify('player disable');
  }

  /**
   * Вкл / выкл плеера
   */
  toggleEnabled(){
    if(this.enable){
      this.setDisable();
    }
    else{
      this.setEnable();
    }
  }

  /**
   * Может ли плеер ходить
   * @param result
   * @returns {boolean}
   */
  canMove(result){
    let output = {};
    if(typeof result === 'object'){ //Если это объект, то значит первый ход
      let resMinusPain = result[0] - this.pain;

      if(resMinusPain <= 0){
        output = {
          status: false,
          reason: 'Количество боли превышает результат броска'
        }
      }
      else{
        if((this.levelNum + resMinusPain) <= 64){
          output = {
            status: true
          }
        }
        else{
          output = {
            status: false,
            reason: 'Вы не можете ходить за край доски'
          }
        }
      }
    }
    else{ //Если это число, тогда плеер сдвинут с помощью других игроков

      //TODO: дописать логику сдвижения другими игроками для мультиплеера
      output = {
        status: false,
        reason: 'Нужно написать логику хода'
      }
    }

    return output;
  }

  /**
   * Удаляем боль у пользователя
   * @param painCount
   */
  removePain(painCount){
    if(this.pain - painCount < 0){
      this.pain = 0;
    }
    else{
      this.pain = this.pain - painCount;
    }
  }

  /**
   * Выставляет в body параметр state
   */
  setStateToBody(){
    $('body').attr('data-state', this.state);
  }
}

export const player = new Player();