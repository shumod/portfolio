'use strict';
import template from 'lodash/template';
import forEach from 'lodash/forEach';
import $ from 'jquery';
import {services} from "./ServiceFunctions";

export default class Board{
  constructor(){}

  /**
   * Отрисовываем поле для игры
   */
  printBoard(){
    let levelsHtml = template(game.data.template.levels)({
      levels: game.data.json.levels,
      forEach
    });
    $('.game-board').html(levelsHtml);
    this.addBaseColors();
  }

  /**
   * Печатаем базовую таблицу со всеми цветами
   */
  addBaseColors(){
    let lineNum = 1,
        linesMax = 8;
    for (lineNum; lineNum <= linesMax; lineNum++){
      for(let levelNum = lineNum * 8 - 7; levelNum < lineNum * 8 + 1; levelNum++){
        let colorText = '000000',
            colorNum = 'ffffff',
            colors = game.data.json.levels.levelMap[levelNum].color;

        this.addColorToLevel(colors, levelNum);
      }

      let colorField = game.data.json.levels.chakras[lineNum].color.field;
      $('.line-' + lineNum + ' .chakra-name, .line-' + lineNum + ' .chakra-sound')
        .css('background-color', services.hexToRGB(colorField));
    }
  }

  /**
   * Делаем кликабельными только те клетки, которые есть в пройденных уровнях игрока.
   */
  addBaseActionsToBoard(){
    let levelmap = game.player.logClass.levelsMap;
    $('.level').attr('onclick', '');

    forEach(levelmap, (count, levelNum) => {
      $(`#level-num-${levelNum}`).addClass('visited').attr('onclick', `services.showLevelDescription(${levelNum})`);
    });
  }

  /**
   * Делаем все клетки на карте кликабельными
   */
  addAllActionsToBoard(){
    for(let levelNum = 0; levelNum <= 64; levelNum++){
      $(`#level-num-${levelNum}`).addClass('visited').attr('onclick', `services.showLevelDescription(${levelNum})`);
    }
  }

  /**
   * Добавляем серые цвета к полю игры
   */
  addGrayExcept41to48(){
    let levelColor = '#ccc',
        levelTextColor = '#fff';

    //$('#levels .level-name, #levels .level-num').css('color', levelTextColor);

    $('.chakra-sound, .chakra-name').css('background-color', levelColor);
    let colorField = game.data.json.levels.chakras[6].color.field;
    $('.line-6 .chakra-name, .line-6 .chakra-sound')
      .css('background-color', services.hexToRGB(colorField));

    $('.level-hexagram div').css('background-color', levelTextColor);
    $('.line-6 .level').addClass('cursor-pointer');

    for (let levelNum = 0; levelNum <= 64; levelNum++){
      $(`#level-num-${levelNum}`)
        .attr('onclick', '').removeClass('visited')
        .css('background-color', levelColor)
        .find('.level-name, .level-num').css('color', levelTextColor);

      if(levelNum >= 41 && levelNum <= 48){
        $(`#level-num-${levelNum}`).attr('onclick', `game.state.chooseLevel(${levelNum})`)
        let colors = game.data.json.levels.levelMap[levelNum].color;
        this.addColorToLevel(colors, levelNum);
      }
    }
  }

  /**
   * Добавляем прозрачность для элементов из лога
   */
  addOpacityFromLog(){
    let levelsmap = game.player.logClass.levelsMap,
        chakrasMap = game.player.logClass.chakrasMap;

    //Находим максимальное значение посещения уровня
    let arr = Object.keys(levelsmap).map(function(key) { return levelsmap[key]; }),
        maxOpacity = Math.max.apply( null, arr );

    $('.level').css('opacity', 0);

    forEach(levelsmap, (opacity, levelNum) => {
      $('#level-num-' + levelNum).css('opacity', opacity / maxOpacity);
    });
  }

  /**
   * Добавляем базовые цвета к уровням
   * @param colors
   * @param levelNum
   */
  addColorToLevel(colors, levelNum){
    let colorText = '000000',
        colorNum = 'ffffff';

    if(colors.field){ $('#level-num-' + levelNum).css('background-color', services.hexToRGB(colors.field)); }

    if(colors.number){ colorNum = colors.number; }
    $('#level-num-' + levelNum + ' .level-num').css('color', services.hexToRGB(colorNum));

    if(colors.text){ colorText = colors.text; }
    $('#level-num-' + levelNum + ' .level-name').css('color', services.hexToRGB(colorText));

    if(colors.hexagram){ $('#level-num-' + levelNum + ' .level-hexagram div')
      .css('background-color', services.hexToRGB(colors.hexagram)) }
  }

  notifyAboutPlayerSave(){
    this.addBaseActionsToBoard();
  }
}