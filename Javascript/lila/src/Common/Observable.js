'use strict';

export default class Observable{
  constructor(){
    this.observers = [];
  }

  attach (Observer){
    this.observers.push(Observer);
  }

  dettach (Observer){
    for(let i in this.observers)
      if(this.observers[i] === Observer)
        this.observers.splice(i, 1)
  }

  notifyAboutDiceStops(result){
    for(let i in this.observers){
      if(typeof this.observers[i].notifyAboutDiceStops === 'function') {
        this.observers[i].notifyAboutDiceStops(result);
      }
    }
  }

  notifyAboutPlayerStopsMove(){
    for(let i in this.observers){
      if(typeof this.observers[i].notifyAboutPlayerStopsMove === 'function') {
        this.observers[i].notifyAboutPlayerStopsMove();
      }
    }
  }

  notifyAboutPlayerSave(){
    for(let i in this.observers){
      if(typeof this.observers[i].notifyAboutPlayerSave === 'function'){
        this.observers[i].notifyAboutPlayerSave();
      }
    }
  }

  notify(data){
    for(let i in this.observers){
      if(typeof this.observers[i].notify === 'function') {
        this.observers[i].notify(data);
      }
    }
  }
}