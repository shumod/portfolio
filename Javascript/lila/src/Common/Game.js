'use strict';

import magnificPopup from 'magnific-popup'; //Нужно для расширения попапа
import $ from 'jquery';
import {services} from "./ServiceFunctions";


import Board from "./Board";

import {player} from "./Player";
import {dice} from "./Dice";

import Move1Interface from "../States/Move1Interface";
import Move2Interface from "../States/Move2Interface";
import Move3Interface from "../States/Move3Interface";
import Move4Interface from "../States/Move4Interface";

import Move1_0Start from "../States/Move1/Move1_0Start";

import Move2_0Start from "../States/Move2/Move2_0Start";
import Move2_21Violence from "../States/Move2/Move2_21Violence";
import Move2_24Serve from "../States/Move2/Move2_24Serve";
import Move2_25Sacrifice from "../States/Move2/Move2_25Sacrifice";
import Move2_38Depression from "../States/Move2/Move2_38Depression";
import Move2_32Joy from "../States/Move2/Move2_32Joy";
import Move2_42Control from "../States/Move2/Move2_42Control";
import Move2_55Imagination from "../States/Move2/Move2_55Imagination";
import Move2_58Realisation from "../States/Move2/Move2_58Realisation";
import Move2_63AchievingGoal from "../States/Move2/Move2_63AchievingGoal";

import Move3_21Violence from "../States/Move3/Move3_21Violence";

import Move4_58Realisation from "../States/Move4/Move4_58Realisation";
import Move4_63AchievingGoal from "../States/Move4/Move4_63AchievingGoal";

import StateLoadData from "../States/Common/StateLoadData";
import StateCreateEntities from "../States/Common/StateCreateEntities";
import StateEnd from "../States/Common/StateEnd";


export default class Game {
  constructor() {
    this.data = {};

    this.board = new Board();
  }

  /**
   *  При старте - начинаем 1-й стейт загрузки данных
   */
  start() {
    $.extend(true, $.magnificPopup.defaults, {
      closeOnBgClick: true,
      showCloseBtn: false,
      enableEscapeKey: false,
      mainClass: 'mfp-fade',
      callbacks: {
        open: () => {
          $('.white-popup').after('<span class="hide-popup" onclick="services.hidePopup()">Посмотреть поле и историю ходов</span>');
        },
        afterClose: function() {
          services.hidePopup();
        },
      }
    });

    this.setStateByName('load data', false);
  }

  /**
   * Установить активный стейт по его имени
   * @param name
   * @param isSave
   */
  setStateByName(name, isSave = true) {
    switch (name) {

      /**
       * Общие состояния игры. Загрузка, создание, окончание
       */
      case 'load data':
        this.state = new StateLoadData();
        break;

      case 'create entities':
        this.state = this.getStateCreateEntities();
        break;

      case 'end':
        this.state = new StateEnd();
        break;

      /**
       * Состояния игры, когда плеер ходит
       */
      case 'move 1':
        this.state = this.getMove1Class();
        break;

      case 'move 2':
        this.state = this.getMove2Class();
        break;

      case 'move 3':
        this.state = this.getMove3Class();
        break;

      case 'move 4':
        this.state = this.getMove4Class();
        break;
    }

    if (isSave) player.setOption('state', name);
    this.state.setActive();
  }

  /**
   * Получить название текущего стейта
   * @returns {*}
   */
  getCurentStateName() {
    return this.state.getName();
  }

  /**
   * Уведомление об остановке хода плеера
   */
  notifyAboutPlayerStopsMove() {
    this.state.notifyAboutPlayerStopsMove();
  }

  /**
   * Уведомление об окончании броска кубика с результатами броска
   * @param result
   */
  notifyAboutDiceStops(result) {
    this.state.notifyAboutDiceStops(result);
  }

  /**
   * Крутим кубик из объекта игры
   * @param count
   */
  rollDice(count){
    dice.roll(count);
  }

  getStateCreateEntities(){
    return new StateCreateEntities();
  }

  /**
   * Получить класс для 1 хода
   * @returns {*}
   */
  getMove1Class(){
    let stateClass;

    switch (player.levelNum){
      case 0:
        stateClass = new Move1_0Start();
        break;
      default:
        stateClass = new Move1Interface();
        break;
    }

    return stateClass;
  }

  /**
   * Получить класс для 2 хода
   * @returns {*}
   */
  getMove2Class(){
    let stateClass;

    switch (player.levelNum){
      case 0:
        stateClass = new Move2_0Start();
        break;

      case 21:
        stateClass = new Move2_21Violence();
        break;

      case 24:
        stateClass = new Move2_24Serve();
        break;

      case 25:
        stateClass = new Move2_25Sacrifice();
        break;

      case 32:
        stateClass = new Move2_32Joy();
        break;

      case 38:
        stateClass = new Move2_38Depression();
        break;

      case 42:
        stateClass = new Move2_42Control();
        break;

      case 55:
        stateClass = new Move2_55Imagination();
        break;

      case 58:
        stateClass = new Move2_58Realisation();
        break;

      case 63:
        stateClass = new Move2_63AchievingGoal();
        break;

      default:
        stateClass = new Move2Interface();
        break;
    }

    return stateClass;
  }

  /**
   * Получить класс для 3 хода
   * @returns {*}
   */
  getMove3Class(){
    let stateClass;

    switch (player.levelNum){
      case 21:
        stateClass = new Move3_21Violence();
        break;

      default:
        stateClass = new Move3Interface();
        break;
    }

    return stateClass;
  }

  /**
   * Получить класс для 4 хода
   * @returns {*}
   */
  getMove4Class(){
    let stateClass;

    switch (player.levelNum){
      case 58:
        stateClass = new Move4_58Realisation();
        break;

      case 63:
        stateClass = new Move4_63AchievingGoal();
        break;

      default:
        stateClass = new Move4Interface();
        break;
    }

    return stateClass;
  }

  movePlayersToTheirLevels(){
    services.animatePlayerMove(this.player.levelNum, 500);
  }
}