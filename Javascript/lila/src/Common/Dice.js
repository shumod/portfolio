'use strict';
import Observable from "./Observable";
import $ from 'jquery';
import {player} from "./Player";
import {services} from "./ServiceFunctions";
import Spritz from 'spritz.js';
import random from 'lodash/random';

class Dice extends Observable{
  constructor(){
    super();
  }

  /**
   * Начиниаем кручение. Если плеер включён в игру, то крутим, иначе выдаём сообщение об ошибке
   * @param count
   * @returns {boolean}
   */
  startRoll(count){
    if(player.enable){
      this.roll(count);
    }
    else{
      services.getDisableAlert();
      return false;
    }
  }

  /**
   * Крутим кубики, возвращаем результат кручения
   * @param count
   */
  roll(count){
    let spriteHeight = 130,
        spriteWidth = 3120,
        innerHtml = '',
        diceResult = [];

    count = count ? count : 1;

    for (let i = 0; i < count; i++) {
      diceResult.push(random(1, 6));
      innerHtml += `<div id="sprite${i}" class="sprite3d"></div>`;
    }

    $.magnificPopup.close();
    $.magnificPopup.open({
      items: {
        src: '<div class="white-popup dice-roll">' + innerHtml + '</div>',
        type: 'inline'
      }
    });

    let sprites = [];
    for (let i = 0; i < count; i++) {
      sprites.push(
        new Spritz('#sprite' + i, {
          picture: [{
            srcset: `${localVars.libraryFolder}${localVars.imagesFolder}/sprite24steps.png`,
            width: spriteWidth,
            height: spriteHeight
          }],
          steps: 24
        }
      ));

      i ? sprites[i].fps(random(8, 12)).play() : sprites[i].fps(random(8, 12)).play('backward');
    }

    $('.sprite3d').css('height', spriteHeight);

    setTimeout(() => {
      for (let i = 0; i < count; i++) {
        sprites[i].destroy();

        let $image = $('<img>');
        $image.attr('src', `${localVars.libraryFolder}${localVars.imagesFolder}/sprite_${diceResult[i]}.png`);
        $image.appendTo('#sprite' + i);
      }

      this.endRoll(diceResult);
    }, random(1500, 2000));
  }

  /**
   * Коллбек на конец кручения кубиков
   * @param result
   */
  endRoll(result){
    this.lastResult = result;

    setTimeout(() => {
      this.notifyAboutDiceStops(result);
    }, 500);
  }

  /**
   * Код для темплейта кнопки для броска кубика
   * @param diceCount - колво кубиков: 1 или 2
   * @returns {{func: string, value: string}}
   */
  getRollButton(diceCount, title){
    diceCount = diceCount ? diceCount : 1;
    title = title ? title : 'Бросать кубик';

    return {
      func: `game.rollDice(${diceCount})`,
      value: title
    }
  }
}

export const dice = new Dice();