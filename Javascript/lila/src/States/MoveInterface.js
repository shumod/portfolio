'use strict';

export default class MoveInterface{
  constructor(){ this.name = 'game move interface' }
  setActive(){console.log('active from move interface')}
  getName(){ return this.name }
  notifyAboutDiceStops(result){ console.log('dice stops' + result) }
  notifyAboutPlayerStopsMove(){ console.log('stop move'); }
  startNewLevel(){ console.log('start new level') }

  /**
   * Выставляем класс для обработки хода. По умолчанию отдаём этот интерфейс
   * @returns {Object}
   */
  state(){
    let state = '';

    switch (this.levelNum){
      default:
        state = this;
        break;
    }

    return state;
  }

  /**
   * Текст с предыдущего действия
   * @returns {number}
   */
  getLastMoveText(){
    console.log('get last move text from interface');
    return 'get last move text from interface';
  }
}