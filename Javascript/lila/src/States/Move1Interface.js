'use strict';

import MoveInterface from "./MoveInterface";
import {player} from "../Common/Player";
import size from "lodash/size";
import {services} from "../Common/ServiceFunctions";
import axios from 'axios';
import {dice} from "../Common/Dice";

export default class Move1Interface extends MoveInterface{
  constructor(){
    super();

    this.name = 'move 1 interface';
    this.levelNum = player.levelNum;
  }

  setActive(){
    if(this.canPlayerMove()){
      this.doAction();
    }
    else{
      axios.get('/get/user/balance').then((response) => {
        let data = response.data;

        if(data.balance >= gamePrice){
          this.showPaymentMessage(data);
        }
        else{
          this.showAddToBalanceMessage(data);
        }
      });
    }
  }

  /**
   * На активации стейта сразу кидаем кубик
   */
  doAction(){
    game.rollDice();
  }

  /**
   * Уведомление о том, что кубик остановился
   * @param result
   */
  notifyAboutDiceStops(result){
    this.playerMoveFrom1DiceRoll(result);
  }

  /**
   * Уведомление об остановке плеера
   * @param result
   */
  notifyAboutPlayerStopsMove(){
    game.setStateByName('move 2', false); //Сохраняем плеера
  }

  /**
   * Двигаемся или стоим после 1-го броска кубика
   * @param result
   */
  playerMoveFrom1DiceRoll(result){
    let canMove = player.canMove(result);

    if(canMove.status){
      player.levelNum += result[0] - player.pain;
      player.logClass.setData1MoveAfterRollDice(result);
      player.moveToLevelNum(player.levelNum);
      player.state = 'move 2';

      player.save();
    }
    else{
      let pain = '';
      if(player.pain > 0){
        pain = ` (-${services.pluralize(player.pain, 'разочарование', 'разочарования', 'разочарований')})`;
      }

      player.logClass.setLast1MoveData({
        description: `Вы выкинули к${result[0]}${pain}.`,
        pain: player.pain
      });

      player.logClass.setLast2MoveData({
        description: `${canMove.reason}. Стойте на месте.`,
        levelNum: player.levelNum,
        result: [0]
      });

      game.setStateByName('move 3', true);
    }
  }

  /**
   * Показываем форму для оплаты
   */
  showPaymentMessage(data){
    let messageAfterHeader =
      `<p>Ваш баланс на данный момент составляет ${data.balance_with_currency}.</p>
         <p>Вы можете продолжить эту игру (со счёта будет списано ${gamePrice}${gameCurrency}) или начать новую, с новой целью.</p>`;

    services.showCleanMessage({
      messageBeforeHeader: '',
      header: 'Тестовая игра закончена',
      messageAfterHeader,
      buttons: [
        {
        func: 'game.state.payFromBalance()',
        value: `Продолжить эту игру (${gamePrice}${gameCurrency})`
        },
        {
          func: `window.open('http://psiho.games/ru/node/add/game_lila_chakra');`,
          value: 'Начать новую игру'
        }
      ]
    });
  }

  showAddToBalanceMessage(data){
    let messageAfterHeader =
        `<p>Ваш баланс на данный момент составляет ${data.balance_with_currency}. 
            Стоимость одной игры: ${gamePrice}${gameCurrency}</p>
         <p>Для продолжения игры пополните баланс.</p>`;

    services.showCleanMessage({
      messageBeforeHeader: '',
      header: 'Тестовая игра закончена',
      messageAfterHeader,
      buttons: [
        {
          func: `window.open('http://psiho.games/ru/user/balance');`,
          value: 'Пополнить баланс'
        },
        {
          func: `game.state.setActive()`,
          value: 'Проверить пополнение'
        }
      ]
    });
  }

  /**
   * Платим за игру, списываем деньги с баланса
   */
  payFromBalance(){

    axios.get(`/games/lila-chakra/pay?nodeid=${nodeId}`).then((response) => {
      let data = response.data;
      if(data.payment === 'success'){
        player.test = false;
        player.save();

        location.reload();
      }
      else{
        alert(data.message);

        switch (data.payment){
          case 'notEnoughMoney':
            game.state.setActive();
            break;

          case 'notLogin':
            location.reload();
        }
      }
    });
  }

  /**
   * Проверяем, может ли юзер ходить, оплачено ли у него игра
   * в тестовом режиме доступно 3 хода
   * @returns {boolean}
   */
  canPlayerMove(){
    let maxTurns = 3;

    if(player.test){
      if(size(player.logClass.data) >= maxTurns){
        return false;
      }
    }

    return true;
  }

}