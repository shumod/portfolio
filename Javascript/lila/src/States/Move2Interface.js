'use strict';

import MoveInterface from "./MoveInterface";
import {player} from "../Common/Player";
import Level from "../Common/Level";
import {services} from "../Common/ServiceFunctions";
import {dice} from "../Common/Dice";

export default class Move2Interface extends MoveInterface{
  constructor(){
    super();

    this.name = 'move 2 interface';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 2-го хода
   */
  setActive(){
    let level = new Level(),
        buttons = [dice.getRollButton(1, 'Бросать ещё раз')],
        nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide2',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result){
    let level = new Level(),
        nextTurns = level.getNextTurn();

    if(nextTurns[result[0]]){
      this.playerMoveFrom2DiceRoll(result, nextTurns);
    }
    else{
      this.playerStayFrom2DiceRoll(result);
    }
  }

  /**
   * Остаёмся на месте после 2-го броска кубика
   * @param result
   */
  playerStayFrom2DiceRoll(result){
    player.logClass.setData2StayAfterRollDice(result);
    game.setStateByName('move 3', true);
  }

  /**
   * Двигаемся после 2-го броска кубика.
   * @param result
   * @param nextTurns
   */
  playerMoveFrom2DiceRoll(result, nextTurns){
    let newLevelNum = +nextTurns[result[0]].levelNum;

    player.logClass.setData2MoveAfterRollDice(result, newLevelNum);
    player.levelNum = newLevelNum;

    player.save();

    player.moveToLevelNum(newLevelNum);
  }

  /**
   * Уведомление о том, что плеер закончил двигаться после 2-го броска кубика
   */
  notifyAboutPlayerStopsMove(){
    game.setStateByName('move 2', false);
  }

  /**
   * Текст предыдущего броска
   * @returns {string}
   */
  getLastMoveText(){
    let log = player.logClass,

        lastTurn = log.getLastTurn(),
        firstMove = log.data[lastTurn].firstMove,
        secondMove = log.data[lastTurn].secondMove,
        secondMoveSize = log.getTurn2MoveSize(),

        text = '',
        pain = firstMove.pain ? ` (-${services.pluralize(firstMove.pain, 'разочарование', 'разочарования', 'разочарований')})` : '';

    if(secondMoveSize === 1){
      if(firstMove.description){ //Если есть сохранённый текст, то выводим его
        text = firstMove.description;
      }
      else{
        let secondLevel = new Level(secondMove[1].levelNum);
        text = `Вы выкинули к${firstMove.result}${pain}, переходите на ${secondLevel.getNumAndName()}`;
      }
    }
    else{
      let prevSecondMoveNum = secondMoveSize - 1;

      if(secondMove[prevSecondMoveNum].description){ //Если есть сохранённый текст, то выводим его
        text = secondMove[prevSecondMoveNum].description;
      }
      else{
        let secondLevel = new Level(secondMove[secondMoveSize].levelNum);
        text = `Вы выкинули к${secondMove[prevSecondMoveNum].result}, переходите на ${secondLevel.getNumAndName()}`;
      }
    }

    return text;
  }
}