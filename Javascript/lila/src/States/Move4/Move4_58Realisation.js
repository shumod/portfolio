'use strict';
import Move4Interface from "../Move4Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";


export default class Move4_58Realisation extends Move4Interface{
  constructor(){
    super();

    this.name = 'move 4 level 58 realisation';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 4-го хода
   */
  setActive(){
    let level = new Level(),
      buttons = [
        {
          func: `game.setStateByName('end', true); services.hidePopup();`,
          value: 'Закончить игру'
        },
        {
          func: `game.state.changeKnowing()`,
          value: 'Изменить выбор'
        }
      ],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide4',
      level,
      nextTurn,
      player,
      buttons
    });
  }
}