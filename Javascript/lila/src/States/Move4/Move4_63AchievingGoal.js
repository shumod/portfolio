'use strict';

import Move4_58Realisation from "./Move4_58Realisation";
import {player} from "../../Common/Player";


export default class Move4_63AchievingGoal extends Move4_58Realisation{
  constructor(){
    super();

    this.name = 'move 4 level 63 achieving goal';
    this.levelNum = player.levelNum;
  }
}