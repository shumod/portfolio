'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";

import capitalize from 'lodash/capitalize';
import {dice} from "../../Common/Dice";

export default class Move2_42Control extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 42 control';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 2-го хода
   */
  setActive(){
    let level = new Level(),
      buttons = [dice.getRollButton()],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide2',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result) {
    player.logClass.setLast2MoveData({result});
    player.save();

    game.board.addGrayExcept41to48();

    if (result[0] === 4 || result[0] === 5) {
      let level = new Level(),
          buttons = [
          {
            func: 'services.hidePopup()',
            value: 'Выбрать'
          }
        ];

      let nextTurn = level.getNextTurn(),
          description = capitalize(nextTurn[result[0]].description);

      let messageAfterHeader = `<p>${description}</p>`;

      services.showCleanMessage({
        messageBeforeHeader: '',
        header: level.getNumAndName(),
        messageAfterHeader,
        buttons
      });
    }
    else {
      parent.notifyAboutDiceStops(result);
    }
  }

  /**
   * Выбрать уровень, на который плеер пойдёт
   * @param levelNum
   */
  chooseLevel(newLevelNum){
    game.board.printBoard();
    player.moveToLevelNum(newLevelNum);

    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      description: `Вы выбрали перейти на @newLevelNum. Остаётесь без продолжения хода.`,
      newLevelNum
    });

    player.levelNum = newLevelNum;
    player.logClass.setNext2MoveData({
      levelNum: player.levelNum,
      result: 0
    });

    player.state = 'move 3';
    player.save();
  }

  /**
   * Продолжения хода после служения нет
   */
  notifyAboutPlayerStopsMove(){
    game.setStateByName('move 3', false);
  }
}