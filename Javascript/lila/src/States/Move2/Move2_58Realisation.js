'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";

export default class Move2_58Realisation extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 58 realisation';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 2-го хода
   */
  setActive(){
    let level = new Level(),
      buttons = [
        {
          func: 'game.state.chooseKnowing()',
          value: 'Выбрать позицию для осознания'
        }
      ],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide2',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  /**
   * Выбор позиции для осознания
   */
  chooseKnowing(){
    player.logClass.setLast2MoveData({
      description: 'Поздравляем! Вы пришли на реализацию цели!',
      result: 0
    });

    player.save();

    game.setStateByName('move 3', false);
  }
}