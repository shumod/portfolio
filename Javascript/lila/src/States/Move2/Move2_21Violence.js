'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import {services} from "../../Common/ServiceFunctions";
import Level from "../../Common/Level";

export default class Move2_21Violence extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 21 violence';
    this.levelNum = player.levelNum;
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result){
    let level = new Level(),
        nextTurns = level.getNextTurn();

    if(nextTurns[result[0]]){
      if(nextTurns[result[0]].levelNum === '-1'){ //Если это неосознанное насилие
        this.playerStayFrom2DiceRoll(result);
        game.setStateByName('move 3', true);
      }
      else{
        this.playerMoveFrom2DiceRoll(result, nextTurns);
      }
    }
    else{
      this.playerStayFrom2DiceRoll(result);
    }
  }
}