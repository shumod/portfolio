'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import {services} from "../../Common/ServiceFunctions";

export default class Move2_32Joy extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 32 joy';
    this.levelNum = player.levelNum;
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result){
    player.removePain(result[0]);

    let pains = services.pluralize(player.pain, 'разочарование', 'разочарования', 'разочарований'),
        have = services.pluralize(player.pain, 'осталась', 'осталось', 'осталось', false);

    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      result,
      description: `Вы уничтожаете разочарования у себя. У вас ${have} ${pains}`
    });

    player.state = 'move 3';

    game.setStateByName('move 3', true);
  }
}