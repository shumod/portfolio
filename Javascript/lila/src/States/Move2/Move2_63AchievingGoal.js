'use strict';

import Move2_58Realisation from "./Move2_58Realisation";
import {player} from "../../Common/Player";

export default class Move2_63AchievingGoal extends Move2_58Realisation{
  constructor(){
    super();

    this.name = 'move 2 level 63 achieving goal';
    this.levelNum = player.levelNum;
  }

  /**
   * Выбор позиции для осознания
   */
  chooseKnowing(){
    player.logClass.setLast2MoveData({
      description: 'Поздравляем! Вы пришли на достижение цели!',
      result: 0
    });

    player.save();

    game.setStateByName('move 3', false);
  }
}