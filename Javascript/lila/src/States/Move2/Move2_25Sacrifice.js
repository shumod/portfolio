'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";
import {dice} from "../../Common/Dice";

export default class Move2_25Sacrifice extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 24 serve';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 2-го хода
   */
  setActive(){
    let level = new Level(),
      buttons = [
        {
          func: `game.state.notSacrifice()`,
          value: 'Не жертвовать'
        },
        dice.getRollButton(1, 'Да, жертвовать')
      ],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide2',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  notSacrifice(){
    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      result: 0,
      description: 'Вы отказались жертвовать и остаётесь на месте'
    });

    game.setStateByName('move 3');
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result){
    let newLevelNum = player.levelNum - result[0];

    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      result
    });

    player.logClass.setNext2MoveData({
      levelNum: newLevelNum,
      result: 0,
      description: 'Вы выбрали жертвовать и падаете на @levels вниз без продолжения хода',
      levels: result[0]
    });

    player.levelNum = newLevelNum;
    player.state = 'move 3';

    player.save();

    player.moveToLevelNum(newLevelNum);
  }

  /**
   * Продолжения хода после служения нет
   */
  notifyAboutPlayerStopsMove(){
    game.setStateByName('move 3', false);
  }
}