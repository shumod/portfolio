'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";
import {dice} from "../../Common/Dice";

export default class Move2_55Imagination extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 55 imagination';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 2-го хода
   */
  setActive(){
    let level = new Level(),
      buttons = [
        dice.getRollButton(2, 'Вообразить!')
      ],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide2',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result){
    let newLevelNum = parseInt(result.join(''));

    switch (newLevelNum){
      case 65:
        newLevelNum = 5;
        break;

      case 66:
        newLevelNum = 6;
        break;

      case 55:
        newLevelNum = 58;
        break;
    }

    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      result
    });

    player.logClass.setNext2MoveData({
      levelNum: newLevelNum,
      result: 0,
      newLevelNum,
      description: 'Вы вообразили и переходите на @newLevelNum без продолжения хода'
    });

    player.levelNum = newLevelNum;
    player.state = 'move 3';

    player.save();

    player.moveToLevelNum(newLevelNum);
  }

  /**
   * Продолжения хода после служения нет
   */
  notifyAboutPlayerStopsMove(){
    game.setStateByName('move 3', false);
  }
}