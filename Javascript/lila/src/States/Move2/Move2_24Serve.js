'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";

export default class Move2_24Serve extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 24 serve';
    this.levelNum = player.levelNum;
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result){
    let newLevelNum = player.levelNum + result[0];

    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      result
    });

    player.logClass.setNext2MoveData({
      levelNum: newLevelNum,
      result: 0,
      description: 'Вы послужили и взлетаете на @levels вверх без продолжения хода',
      levels: result[0]
    });

    player.levelNum = newLevelNum;
    player.state = 'move 3';

    player.save();

    player.moveToLevelNum(newLevelNum);
  }

  /**
   * Продолжения хода после служения нет
   */
  notifyAboutPlayerStopsMove(){
    game.setStateByName('move 3', false);
  }
}