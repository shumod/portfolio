'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";
import {services} from "../../Common/ServiceFunctions";
import escape from 'lodash/escape';

export default class Move2_0Start extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 38 depression';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 2-го хода
   */
  setActive(){
    let level = new Level(0),
      buttons = [
        {
          func: 'game.state.changeTarget()',
          value: 'Сменить цель'
        },
        {
          func: 'game.state.stayAndGetPain()',
          value: 'Не менять цель'
        }
      ];

    let specialDesc = level.getSpecial('description'),
        specialDescReplace = specialDesc.replace('@pain', services.pluralize(player.pain + 1, 'клетку', 'клетки', 'клеток'));

    let messageAfterHeader = `<p class="level-description">${specialDescReplace}</p>
                            <h3>Ваша текущая цель:</h3>
                            <p>${player.target}</p>`;

    services.showCleanMessage({
      messageBeforeHeader: `<p>Вы падаете на ${level.getNumAndName()}`,
      header: level.getNumAndName(),
      messageAfterHeader,
      buttons
    });
  }

  /**
   * Сообщение о смене цели
   */
  changeTarget(){
    let level = new Level(0);
    let buttons = [
          {
            func: 'game.state.saveNewTarget()',
            value: 'Сохранить новую цель'
          },
          {
            func: 'game.state.setActive()',
            value: 'Передумать'
          }
        ];


    let specialDesc = level.getSpecial('description'),
        specialDescReplace = specialDesc.replace('@pain', services.pluralize(player.pain + 1, 'клетку', 'клетки', 'клеток')),
        messageAfterHeader =
          `<p class="level-description">${specialDescReplace}</p>
           <h3>Напишите новую цель:</h3>
           <p>
             <textarea id="new-target"></textarea>
           </p>`;

    services.showCleanMessage({
      messageBeforeHeader: '',
      header: level.getNumAndName(),
      messageAfterHeader,
      buttons
    });
  }

  /**
   * Сохраняем новую цель
   */
  saveNewTarget(){
    let newTarget = document.getElementById('new-target').value;
    if(newTarget.trim() !== ''){
      player.changeTarget(escape(newTarget));

      player.logClass.setLast2MoveData({
        levelNum: player.levelNum,
        result: 0,
        description: `Разочарование не наступило, так как вы сформулировали новую цель: @newTarget`,
        newTarget
      });

      game.setStateByName('move 3', true);
    }
    else{
      alert('Напишите новую цель. Она не может быть пустой');
    }
  }

  /**
   * Остаться на месте и получить разочарование
   */
  stayAndGetPain(){
    player.pain++;

    let pains = services.pluralize(player.pain, 'разочарование', 'разочарований', 'разочарований');

    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      result: 0,
      description: `Вы остаётесь на месте и получаете +1 разочарование. На данный момент у вас ${pains}`
    });

    game.setStateByName('move 3', true);
  }
}