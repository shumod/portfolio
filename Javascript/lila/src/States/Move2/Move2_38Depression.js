'use strict';
import Move2Interface from "../Move2Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";
import {dice} from "../../Common/Dice";

export default class Move2_38Depression extends Move2Interface{
  constructor(){
    super();

    this.name = 'move 2 level 38 depression';
    this.levelNum = player.levelNum;
  }

  /**
   * В начале стейта показываем карточку для 2-го хода
   */
  setActive(){
    let level = new Level(),
      buttons = [
        dice.getRollButton(1, 'Бросать ещё раз')
      ],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide2',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  /**
   * Уведомление об остановке кубиков
   * @param result
   */
  notifyAboutDiceStops(result){
    let newLevelNum = player.levelNum - result[0];

    player.logClass.setLast2MoveData({
      levelNum: player.levelNum,
      result,
    });

    player.logClass.setNext2MoveData({
      levelNum: newLevelNum,
      levels: result[0],
      description: 'Вы находитесь в депрессии и падаете на @levels вниз без продолжения хода'
    });

    player.levelNum = newLevelNum;
    player.state = 'move 3';

    player.save();

    player.moveToLevelNum(newLevelNum);
  }

  /**
   * Продолжения хода после служения нет
   */
  notifyAboutPlayerStopsMove(){
    game.setStateByName('move 3', false);
  }
}