'use strict';

import Move3Interface from "../Move3Interface";
import {player} from "../../Common/Player";
import Level from "../../Common/Level";

export default class Move3_21Violence extends Move3Interface{
  constructor(){
    super();

    this.name = 'move 3 level 21 violence';
    this.levelNum = player.levelNum;
  }

  setActive(){
    let lastTurnNum = player.logClass.getLastTurn(),
        lastSecondMoveNum = player.logClass.getTurn2MoveSize(lastTurnNum),
        log = player.logClass.data,
        secondMove = log[lastTurnNum].secondMove[lastSecondMoveNum];

    if(secondMove.levelNum === 21 && secondMove.result[0] === 5 && !secondMove.description){
      services.showCleanMessage({
        messageBeforeHeader: '<p>Насилие вы совершаете неосознанно</p>',
        header: 'Ждите следующего хода <span id="timer"></span>',
        messageAfterHeader: '<div id="stop-waiting-text"></div>',
        buttons: []
      });

      setTimeout(() => {
        this.startTimer();
      }, 200);
    }
    else{
      super.setActive();
    }
  }

  /**
   * Запускаем таймер
   */
  startTimer(){
    if(!window.timerObj){
      window.timerObj = {
        tmp: 60 * 5,
        restrictTmp(value){
          this.tmp -= value;

          let timer = document.getElementById('timer'),
              m = (this.tmp / 60) >> 0,
              s = (this.tmp - m * 60) + '';

          if(timer){
            timer.textContent = m + ':' + (s.length > 1 ? '' : '0') + s;
          }

          return this.tmp;
        },
        get getTmp(){
          return this.tmp;
        }
      };

      window.timerObj.interval = setInterval(() => {
        window.timerObj.restrictTmp(1);
        window.timerObj.getTmp !== -1 || this.stopTimer(window.timerObj.interval);
      }, 1000);
    }
  }

  /**
   * Остановка таймера
   * @param timerInterval
   */
  stopTimer(timerInterval){
    clearInterval(timerInterval);
    let timer = document.getElementById('timer'),
        stopWaitingText = document.getElementById('stop-waiting-text');

    if(timer){
      timer.innerHTML =  '';
      stopWaitingText.innerHTML =  'Ваше ожидание закончено';
    }

    this.stopWaiting();
  }

  /**
   * Функция на окончание ожидания
   */
  stopWaiting(){
    let logClass = player.logClass;

    logClass.setLast2MoveData({
      description: 'Вы совершили неосознанное насилие'
    });

    player.save();

    setTimeout(() => {
      game.state.setActive();
    }, 1000);
  }
}