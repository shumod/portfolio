'use strict';
import MoveInterface from "./MoveInterface";

import {player} from "../Common/Player";
import Level from "../Common/Level";
import Log from "../Common/Log";

import forEach from "lodash/forEach";

export default class Move3Interface extends MoveInterface{
  constructor(){
    super();

    this.name = 'move 3 interface';
    this.levelNum = player.levelNum;
  }

  setActive(){
    let level = new Level(),
      buttons = [{
        func: `game.state.setKnowing()`,
        value: 'Выбрать позицию для осознания'
      }],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide3',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  /**
   * Сохраняем позицию для осознания на 3-м ходу
   */
  setKnowing(){
    let checked = $('input[name=knowing]:checked', '#knowing-form').val();

    if(!checked){
      alert('Выберите позицию для осознания');
    }
    else{
      player.logClass.setKnowing(checked);
      game.setStateByName('move 4', true);
    }
  }

  /**
   * Текст предыдущего броска
   * @returns {string}
   */
  getLastMoveText(){
    let log = player.logClass,

        lastTurn = log.getLastTurn(),
        secondMove = log.data[lastTurn].secondMove,
        secondMoveSize = log.getTurn2MoveSize(),
        prevSecondMoveNum = secondMoveSize - 1,

        text = '';

    if(secondMoveSize > 0 && secondMove[secondMoveSize].description){ //Если есть сохранённый текст, то выводим его
      let d = secondMove[secondMoveSize].description;
      let matches = d.match(/@[\w]{0,}/ig);

      forEach(matches, (match) => {
        match = match.replace('@', '');
        d = Log.getMatchText(d, match, secondMove[secondMoveSize]);
      });

      text = d;
    }
    else{
      let secondLevel = new Level(secondMove[secondMoveSize].levelNum);
      text = `Вы выкинули к${secondMove[secondMoveSize].result}, остаётесь на ${secondLevel.getNumAndName()}`;
    }

    return text;
  }
}