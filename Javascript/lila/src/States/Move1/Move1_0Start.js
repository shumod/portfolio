'use strict';

import Move1Interface from "../Move1Interface";
import Level from "../../Common/Level";
import {player} from "../../Common/Player";
import {dice} from "../../Common/Dice";


export default class Move1_0Start extends Move1Interface{
  constructor(){
    super();

    this.name = 'move 1 level 0 start';
    this.levelNum = player.levelNum;
  }

  /**
   * Показываем первую сторону карточки.
   */
  doAction(){
    let level = new Level(),
      buttons = [dice.getRollButton()],
      messageAfterHeader = '';

    messageAfterHeader = `<p>${level.getDescription()}</p>
                        <h3>Ваша текущая цель:</h3>
                        <p>${player.target}</p>`;

    services.showCleanMessage({
      messageBeforeHeader: '',
      header: level.getNumAndName(),
      messageAfterHeader: messageAfterHeader,
      buttons
    });
  }
}