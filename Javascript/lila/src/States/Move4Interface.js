'use strict';
import MoveInterface from "./MoveInterface";
import {player} from "../Common/Player";
import Level from "../Common/Level";

export default class Move4Interface extends MoveInterface{
  constructor(){
    super();

    this.name = 'move 4 interface';
    this.levelNum = player.levelNum;
  }

  setActive(){
    let level = new Level(),
      buttons = [
        {
          func: `game.state.startNewTurn()`,
          value: 'Продолжить игру'
        },
        {
          func: `game.state.changeKnowing()`,
          value: 'Изменить выбор'
        }
      ],
      nextTurn = level.getNextTurn();

    services.showMessage({
      template: 'cardSide4',
      level,
      nextTurn,
      player,
      buttons
    });
  }

  changeKnowing(){
    player.logClass.removeLastKnowing();
    game.setStateByName('move 3');
  }

  /**
   * Начинаем новый ход. Добавляем в лог и ставим игру на 1 move
   */
  startNewTurn(){
    player.logClass.startNewTurn();
    game.setStateByName('move 1', true);
  }
}