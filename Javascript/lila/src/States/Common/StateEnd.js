'use strict';
import StateInterface from "../StateInterface";
import Level from "../../Common/Level";

export default class StateEnd extends StateInterface{
  constructor(){
    super();
    this.name = 'end';
  }

  setActive(){
    let level = new Level();

    document.querySelector('.game-description').innerHTML =
      `<strong>Поздавляем! Игра завершена</strong>
       <div>Теперь вы можете проанализировать историю своих ходов</div>
       <div>На поле отображены клетки, по которым вы ходили. Чем чаще была посещена клетка, тем она ярче выглядит.</div>`;
    game.board.addOpacityFromLog();
    document.querySelector('.show-popup').style.display = 'none';
  }
}