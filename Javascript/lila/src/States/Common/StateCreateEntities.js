'use strict';
import StateInterface from "../StateInterface";
import {player} from "../../Common/Player";
import {dice} from "../../Common/Dice";


export default class StateCreateEntities extends StateInterface {
  constructor(){
    super();
    this.name = 'create entities';

    this.loadPlayerFromDB = localVars.isLocal ? game.data.json.player : game.data.player;
  }

  /**
   * Запускается сразу после включения стейта
   */
  setActive(){
    game.board.printBoard();
    this.createPlayer();
    this.attachObservers();
  }

  /**
   * Создаём плеера
   */
  createPlayer(){
    let playerObject = this.loadPlayer();

    if(playerObject.revision === 0){
      playerObject.target = playerData.target;
    }

    if((playerObject.test === undefined || playerObject.test === true) && playerData.test){
      playerObject.test = true;
    }

    player.createPlayer(playerObject);
    player.logClass.print();
    document.querySelector('.game-target').innerHTML = player.target;
  }

  /**
   * Грузим плеера из локал сторадж или из БД.
   * @returns {*}
   */
  loadPlayer(){
    let loadPlayerFromLS = JSON.parse(localStorage.getItem('player_' + playerData.nid));

    if(loadPlayerFromLS && loadPlayerFromLS.revision > this.loadPlayerFromDB.revision){
      return loadPlayerFromLS;
    }
    else{
      return this.loadPlayerFromDB;
    }
  }

  loadPlayerFromDB(){
    return '';
  }

  attachObservers(){
    dice.attach(game);
    player.attach(game.board);
  }

  notifyAboutPlayerStopsMove(){
    //После движения плеера начинаем новый уровень
    setTimeout(() => {
      game.setStateByName(player.state, false);
    }, 100);
  }

}