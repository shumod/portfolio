'use strict';

import StateInterface from "../StateInterface";
import axios from 'axios';
import {strings} from "../../Common/Strings";

import camelCase from 'lodash/camelCase';

export default class StateLoadData extends StateInterface{
  constructor(){
    super();
    this.name = 'load data';

    this.dataMap = {
      json: ['levels', 'player', 'strings'],
      template: ['levels', 'message', 'cardSide1', 'cardSide2', 'cardSide3', 'cardSide4', 'log']
    };

    this.loadedCount = 0;

    this.data = {
      json: {},
      template: {},
      player: {}
    };
  }

  setActive(){
    this.loadDataStart();
  }

  /**
   * Грузим изображения и данные
   */
  loadDataStart(){
    this.request(`/games/load-player?nid=${playerData.nid }`, 'player');

    for (let i = 0; i < this.dataMap.json.length; i++){
      this.request(localVars.libraryFolder + '/json/' + this.dataMap.json[i] + '.json', 'json', this.dataMap.json[i]);
    }

    for (let i = 0; i < this.dataMap.template.length; i++){
      this.request(localVars.libraryFolder + '/template/' + this.dataMap.template[i] + '.tpl.txt', 'template', camelCase(this.dataMap.template[i]));
    }

    let imgSrc = localVars.libraryFolder + localVars.imagesFolder;
    setTimeout(() => {
      StateLoadData.preloadImages(
        imgSrc + '/sprite_1.png',
        imgSrc + '/sprite_2.png',
        imgSrc + '/sprite_3.png',
        imgSrc + '/sprite_4.png',
        imgSrc + '/sprite_5.png',
        imgSrc + '/sprite24steps.png',
      );
    }, 1500);


  }

  //Закончили грузить данные
  loadDataEnd(){
    let elem = document.querySelector('.game-description');
    elem.innerHTML = '';

    game.data = this.data;
    game.setStateByName('create entities', false);

    strings.setData();
  }

  // Запрашиваем данные
  request(url, type, name = ''){
    axios.get(url)
      .then((response) => {
        switch (type){
          case 'json':
          case 'template':
            this.data[type][name] = response.data;
            break;

          case 'player':
            this.data.player = response.data;
            break;
        }
        this.loadedCount++;

        // +1 потому что кроме json и template'ов грузим ещё плеера
        if(this.loadedCount === (this.dataMap.json.length + this.dataMap.template.length + 1)){
          this.loadDataEnd();
        }
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  /**
   * Функция для загрузки картинок, чтобы они уже отображались сразу
   * @param args
   */
  static preloadImages(...args) {
    for (let i = 0; i < args.length; i++) {
      let image = new Image();
      image.src = arguments[i];
    }
  }
}