'use strict';

export default class StateInterface{
  constructor(){ this.name = 'game state interface' }
  setActive(){console.log('active from interface')}
  getName(){ return this.name }
  notifyAboutDiceStops(result){ console.log('dice stops' + result) }
  notifyAboutPlayerStopsMove(){ console.log('stop move'); }
}