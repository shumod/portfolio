import MultiGame from "./Multiplayer/MultiGame";

window.game = new MultiGame();

document.addEventListener('DOMContentLoaded', () => {
  game.start();
});

/*
 * Player
 */

game.socket.on('player connected', function(player){ game.playerConnected(player) });
game.socket.on('player disconnected', function(player){ game.playerDisconnected(player); });

// Получаем информацию о других плеерах
game.socket.on('players all data', function(players){ game.multiGameFor.allData(players); });

//Окончание движения плеера
game.socket.on('player stops move', function(data){ game.multiGameFor.playerStopsMove(data) });

//Вкл-выкл плеера
game.socket.on('player enable', function(socketId){ game.multiGameFor.enablePlayer(socketId); });
game.socket.on('player disable', function(socketId){ game.multiGameFor.disablePlayer(socketId); });


/*
socket.on('player saved', function(socketId){  multiGame.playerType.playerSaved(socketId); });
*/

/**
 * Chat
 */
game.socket.on('chat print message', function(data){ game.chat.printMessage(data); });
game.socket.on('chat player printing', function(message){ game.chat.playerPrinting(message); });