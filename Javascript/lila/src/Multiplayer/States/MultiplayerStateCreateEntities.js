'use strict';

import StateCreateEntities from "../../States/Common/StateCreateEntities";
import MultiTrainer from "../MultiTrainer";
import TrainerState from "./TrainerState";

export default class MultiplayerStateCreateEntities extends StateCreateEntities {
  constructor(){
    super();
    this.name = 'multiplayer create entities';
  }

  /**
   * Запускается сразу после включения стейта
   * Если это игрок, то создаём его как обычно
   */
  setActive(){
    if(multiplayerData.type !== 'trainer'){
      super.setActive();
    }
    else{
      game.board.printBoard();
      game.board.addAllActionsToBoard();
      document.querySelector('.show-popup').remove();

      game.player = new MultiTrainer();

      game.state = new TrainerState();

      game.player.notify('player created');
    }
  }
}