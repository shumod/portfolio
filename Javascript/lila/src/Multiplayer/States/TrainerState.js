'use strict';
import StateInterface from "../../States/StateInterface";
import {services} from "../../Common/ServiceFunctions";

export default class TrainerState extends StateInterface{
  constructor(){
    super();
    this.name = 'trainer state'
  }

  setActive(){
    services.hidePopup();
  }
}