import Game from "../Common/Game";

import MultiplayerStateCreateEntities from "./States/MultiplayerStateCreateEntities";

import MultiGameForPlayer from "./MultiGameFor/MultiGameForPlayer";
import MultiGameForTrainer from "./MultiGameFor/MultiGameForTrainer";

import {services} from "../Common/ServiceFunctions";
import {dice} from "../Common/Dice";

import io from 'socket.io-client';
import Chat from "./Chat/Chat";

import forOwn from 'lodash/forOwn';
import merge from 'lodash/merge';

export default class MultiGame extends Game{
  constructor(gameObject){
    super();

    this.multiPlayers = {};
    this.chat = new Chat();
    this.setMultiplayer();

    this.multiplayerData = merge(multiplayerData, playerData);

    this.socket = io.connect('http://localhost:8080');

    document.querySelector('body').classList.add('multiplayer');
  }

  setMultiplayer(){
    switch (multiplayerData.type){
      case 'player':
        this.multiGameFor = new MultiGameForPlayer();
        break;

      case 'trainer':
        this.multiGameFor = new MultiGameForTrainer();
        break;
    }
  }

  getStateCreateEntities(){
    return new MultiplayerStateCreateEntities();
  }

  /**
   * Получить класс для 3 хода
   * @returns {*}
   */
  getMove3Class(){
    this.player.setDisable();
    return super.getMove3Class();
  }

  /**
   * Крутим кубик из объекта игры
   * @param count
   */
  rollDice(count){
    if(this.player.enable){
      dice.roll(count);
    }
    else{
      services.showCleanMessage({
        messageBeforeHeader: '',
        header: '',
        messageAfterHeader: 'Сейчас ходят другие игроки, дождись своего хода',
        buttons: [{
          value: 'OK',
          func: 'services.hidePopup()',
        }]
      });
    }
  }

  /**
   * Событие, когда плеер подключён
   * @param player
   */
  playerConnected(player){
    if(player.socketId !== this.socket.id){ // Если этот плеер не я
      this.multiGameFor.somePlayerConnected(player);
    }
    else{ //Если этот игрок - я, то спрашиваем о других игроках и пишем в чат, информацию о подключении
      this.multiGameFor.sendToChatAboutEnterGame(player.name);
      this.socket.emit('send me other players');
    }
  }

  /**
   * Событие, когда плеер отключён
   * @param player
   */
  playerDisconnected(player){
    if(this.multiPlayers[player.socketId]){
      this.multiPlayers[player.socketId].remove();
      delete this.multiPlayers[player.socketId];
    }
  }

  /**
   * Уведомления обсерверам
   * @param data
   */
  notify(data){
    switch (data) {
      case 'player created':
        this.multiplayerData.levelNum = services.isNumeric(this.player.levelNum) ? this.player.levelNum : '';
        this.socket.emit('send player', this.multiplayerData);

        game.player.setDisable();

        break;

      case 'player enable':
        this.socket.emit('player enable', this.socket.id);
        break;

      case 'player disable':
        this.socket.emit('player disable', this.socket.id);
        break;
    }
  }

  /**
   * Уведомление об остановке хода плеера
   */
  notifyAboutPlayerStopsMove() {
    super.notifyAboutPlayerStopsMove();

    this.socket.emit('player stops move', this.player.levelNum);
  }

  movePlayersToTheirLevels(){
    super.movePlayersToTheirLevels(); //Двигаем меня

    forOwn(this.multiPlayers, (multiplayer) => {
      multiplayer.moveToLevelNum();
    });
  }
}


