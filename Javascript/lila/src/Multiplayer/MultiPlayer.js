'use strict';

import $ from 'jquery';
import forOwn from 'lodash/forOwn';
import Level from "../Common/Level";

export default class MultiPlayer{
  constructor(data){
    forOwn(data, (value, key) => {
      this[key] = value;
    });

    this.createImage();

    setTimeout(() => {
      this.moveToLevelNum();
    }, 500);

    game.multiGameFor.addPlayerData(this);
  }

  createImage(){
    let playerImage = new Image();
    playerImage.src = this.imageSrc;
    playerImage.classList.add('player', `player-${this.socketId}`, 'player-image', `player-data-${this.socketId}`);

    document.querySelector('body').appendChild(playerImage);

    this.image = playerImage;
  }

  moveToLevelNum(levelNum, isResize = false){
    if(!levelNum){
      levelNum = this.levelNum;
    }

    let level = new Level(levelNum),
        levelCoords = level.getLevelCoords();

    let imagePos = this.image.getBoundingClientRect();

    this.image.style.top =
      levelCoords.top + (levelCoords.height / 2) - (imagePos.height / 2) + 'px';
    this.image.style.left =
      levelCoords.left + (levelCoords.width / 2) - (imagePos.width / 2) + 'px';
  }

  remove(){
    $('.player-data-' + this.socketId).remove();
  }
}