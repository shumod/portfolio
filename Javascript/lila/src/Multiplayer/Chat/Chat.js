import $ from 'jquery';
import debounce from 'lodash/debounce';
import delay from 'lodash/delay';

/*
 * Чатик
 */
export default class Chat{
  constructor(){
    this.printChat();
  }

  printChat(){
    $('#print-chat').append(
      '    <div class="chat-open"></div>\n' +
      '    <div class="close chat-wrapper">\n' +
      '      <div class="chat-close">✖</div>\n' +
      '      <div class="chat-header">Чат</div>\n' +
      '      <div id="chat">\n' +
      '        <ul></ul>\n' +
      '        <div id="printing"></div>\n' +
      '      </div>\n' +
      '      <form><input type="text" id="text-send-chat"><button id="send-message" onclick="game.chat.sendMessage(); return false;">Отправить</button></form>\n' +
      '    </div>\n'
    );

    $('.chat-open').on('click', function(){
      $('.chat-wrapper').removeClass('close');
    });

    $('.chat-close').on('click', function(){
      $('.chat-wrapper').addClass('close');
    });

    $('#text-send-chat').on('keyup', debounce( () => {
      game.socket.emit('chat printing', '');
    }, 300, { 'maxWait': 1500 }));
  }

  sendMessage(){
    let textSendChat = $('#text-send-chat');

    if(textSendChat.val() !== ''){
      game.socket.emit('chat send message', textSendChat.val());
      textSendChat.val('').focus();
    }
  }

  printMessage(data){
    let message = '',
      playerName = '<strong>' + data.name + ': </strong> ',
      time = '<span class="time">' + this.currentTime() + '</span>';

    if(data.type === 'message'){
      message = time + playerName + data.message;

      $('#chat ul')
        .append($('<li class="message" style="display: none">')
          .html(message));

      $('.message').fadeIn(300);
      $('#printing').hide();
    }
    else if(data.type === 'notification'){
      message = time + data.message;

      $('.notification').remove().fadeIn(300);

      $('#chat ul')
        .append($('<li class="notification" style="display: none">')
          .html(time + data.message));
    }

    $('.chat-open').html($('<div class="chat-open-wrapper"></div>').html(message));

    //Перематываем вниз
    document.querySelector('#chat').scrollTop = 9999;
  }

  playerPrinting(message){
    let printing = $('#printing');

    printing
      .fadeIn(300)
      .text(message);

    delay(function(text) {
      printing.fadeOut(500);
    }, 1000);
  }

  currentTime(){
    let time = new Date();

    let output = ("0" + time.getHours()).slice(-2) + ":" +
      ("0" + time.getMinutes()).slice(-2);

    return output;
  }
}