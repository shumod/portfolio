'use strict';
import Observable from "../Common/Observable";

export default class MultiTrainer extends Observable{
  constructor(){
    super();

    this.createData();
    this.attach(game);
  }

  createData(){
    document.querySelector('body').classList.add('trainer');
    this.logClass = this.getLogClass();
  }

  getLogClass(){
    let map = {
      levelsMap: {}
    };

    for(let i = 0; i <= 64; i++){
      map.levelsMap[i] = i;
    }

    return map;
  }


  moveToLevelNum(){}
  setDisable(){}
}