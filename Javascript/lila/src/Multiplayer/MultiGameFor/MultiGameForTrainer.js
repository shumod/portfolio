'use strict';
import MultiGameForInterface from "./MultiGameForInterface";
import axios from 'axios';
import MultiLog from "../MultiLog";

export default class MultiGameForTrainer extends MultiGameForInterface{
  constructor(){
    super();
  }

  /**
   * Отправляем инфо в чат о входе в игру
   * @param name
   */
  sendToChatAboutEnterGame(name){
    game.socket.emit('chat send message', 'Ведущий в игре: ' + name, 'notification');
  }

  /**
   * Добавляем информацию о плеере
   * @param multiPlayer
   */
  addPlayerData(multiPlayer){
    this.addPlayerName(multiPlayer);
    this.sortPlayersNames();

    this.addPlayerLog(multiPlayer);
  }

  /**
   * Добавляем имя плеера для лога и переключения активности
   * @param multiPlayer
   */
  addPlayerName(multiPlayer){
    let socketId = multiPlayer.socketId;

    /**
     * Создаём контейнеры для имён пользователя
     */
    if (document.querySelector('.game-log .names') === null) {
      let toggleNames = document.createElement('div'),
          insertBeforeElement = document.querySelector('.game-log');

      toggleNames.className = 'toggle-names';
      let parentElement = insertBeforeElement.parentNode;
      parentElement.insertBefore(toggleNames, insertBeforeElement);

      let logNames = document.createElement('div');
      logNames.className = 'names';
      document.querySelector('.game-log').appendChild(logNames);
    }


    /**
     * Вставляем в контейнеры имена
     * Сначала переключение, потом для лога
     */
    let toggleName = document.createElement('div');
    toggleName.classList.add('player-toggle-name', 'player-toggle-name-' + socketId, 'player-data-' + socketId);
    toggleName.setAttribute('data-nid', multiPlayer.nid);
    toggleName.setAttribute('onclick', 'game.multiGameFor.togglePlayer("' + socketId + '")');
    toggleName.innerText = multiPlayer.name;
    document.querySelector('.toggle-names').appendChild(toggleName);


    let logName = document.createElement('div');
    logName.classList.add('player-log-name', 'player-log-name-' + socketId, 'player-data-' + socketId);
    logName.setAttribute('data-nid', multiPlayer.nid);
    logName.setAttribute('onclick', 'game.multiGameFor.showLog("' + socketId + '")');
    logName.innerText = multiPlayer.name;
    document.querySelector('.game-log .names').appendChild(logName);
  }

  /**
   * Получаем лог пользователя из админки и печатаем его
   * @param multiPlayer
   */
  addPlayerLog(multiPlayer){
    axios.get(`/games/load-player?nid=${multiPlayer.nid}`)
      .then((response) => {
        this.printPlayerLog(multiPlayer, response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  /**
   * Печать лога плеера
   * @param multiPlayer
   * @param logData
   */
  printPlayerLog(multiPlayer, logData){
    if(logData){
      multiPlayer.logClass = new MultiLog(logData.log);

      if (document.querySelector('.game-log .logs') === null) {
        let logs = document.createElement('div');
        logs.className = "logs";

        document.querySelector('.game-log').appendChild(logs);
      }

      let log = document.createElement('div');
      log.classList.add('hide', 'player-log', 'player-data-' + multiPlayer.socketId, 'player-log-' + multiPlayer.socketId);

      document.querySelector('.game-log .logs').appendChild(log);

      multiPlayer.logClass.print('.player-log-' + multiPlayer.socketId);
    }
  }

  /**
   * Показать лог при клике на имя пользователя
   * @param socketId
   */
  showLog(socketId){
    $('.player-log').addClass('hide').removeClass('show');
    $('.player-log-' + socketId).addClass('show').removeClass('hide');
  }

  /**
   * Сортировка имён пользователя
   */
  sortPlayersNames(){
    let parents = ['.game-log .names', '.toggle-names'];

    for(parent of parents){
      let namesParent = document.querySelector(parent),
          names = [].slice.call(namesParent.children);

      let sorted = names.sort(function(a, b) {
        return a.getAttribute('data-nid') - b.getAttribute('data-nid');
      });

      sorted.forEach(child => namesParent.appendChild(child));
    }
  }

  enablePlayer(socketId){
    super.enablePlayer(socketId);

    document.querySelector('.player-data-' + socketId).classList.add('enabled');
  }

  disablePlayer(socketId){
    super.disablePlayer(socketId);

    if(document.querySelector('.player-data-' + socketId)){
      document.querySelector('.player-data-' + socketId).classList.remove('enabled');
    }
  }

  /**
   * Включить-выключить плеера.
   * @param socketId
   */
  togglePlayer(socketId){
    if(document.querySelector('.player-data-' + socketId).classList.contains('enabled')){
      game.socket.emit('player disable', socketId);
    }
    else{
      game.socket.emit('player enable', socketId);
    }
  }
}