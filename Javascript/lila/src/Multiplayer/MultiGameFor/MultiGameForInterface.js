'use strict';
import MultiPlayer from "../MultiPlayer";
import $ from 'jquery';

export default class MultiGameForInterface{
  constructor(){}
  sendToChatAboutEnterGame(){}
  addPlayerData(){}

  allData(players){
    for (let playerId in players){
      if(!game.multiPlayers[playerId]){
        if(playerId !== game.socket.id && players[playerId].type !== 'trainer'){ //Если это не я и не тренер, то добавляем в список мультиигроков
          game.multiPlayers[playerId] = new MultiPlayer(players[playerId]);
        }
      }
    }
  }

  somePlayerConnected(player){
    if(player.type !== 'trainer'){
      game.multiPlayers[player.socketId] = new MultiPlayer(player);
    }
  }

  playerStopsMove(data){
    if(data.socketId !== game.socket.id){ //Если этот плеер - не я
      let playerWhoStop = game.multiPlayers[data.socketId];
      playerWhoStop.levelNum = data.newLevelNum;
      playerWhoStop.moveToLevelNum();

      $('.player-name-' + data.socketId).removeClass('active');
      $('#player-image-' + data.socketId).removeClass('active');
    }
  }

  enablePlayer(socketId){
    $('.player-' + socketId).addClass('active');
  }

  disablePlayer(socketId){
    $('.player-' + socketId).removeClass('active');
  }
}