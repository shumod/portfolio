'use strict';
import MultiGameForInterface from "./MultiGameForInterface";
import MultiPlayer from "../MultiPlayer";

export default class MultiGameForPlayer extends MultiGameForInterface{
  constructor(){
    super();
  }

  sendToChatAboutEnterGame(name){
    game.socket.emit('chat send message', 'Новый игрок зашёл в игру: ' + name, 'notification');
  }

  enablePlayer(socketId){
    if(socketId === game.socket.id){ //Если пытаются включить меня
      game.player.setEnable();
    }
    else{
      super.enablePlayer(socketId);
    }
    console.log('2')
  }

  disablePlayer(socketId){
    if(socketId === game.socket.id){ //Если пытаются включить меня
      game.player.setDisable();
    }
    else{
      super.disablePlayer(socketId);
    }
    console.log('1')
  }
}