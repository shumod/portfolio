let socket = require('socket.io'),
    express = require('express'),
    http = require('http'),
    app = express(),
    server = http.createServer(app),
    io = socket.listen(server),
    players = {};

require('./serverchat.js')(io, players);
require('./serverplayer.js')(io, players);

server.listen(8080);