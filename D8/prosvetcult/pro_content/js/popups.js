(function ($) {
  'use strict';

  Drupal.behaviors.popups = {
    attach: function(context, settings) {
      $('.magnific-popup').magnificPopup({
        type: 'image'
      });

      $('.magnific-popup-gallery').magnificPopup({
        type: 'image',
        gallery: {
          enabled: true
        }
      });
    }
  };

}(jQuery));