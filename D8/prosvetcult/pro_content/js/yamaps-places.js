Drupal.behaviors.yamaps_places = {
  attach: function(context, settings) {
    ymaps.ready(init);
    var map, placemark, collection,
        mapData = settings.map,
        zoom = 14;

    function init(){
      map = new ymaps.Map("ymap", {
        center: [55.76, 37.64],
        zoom: zoom
      });

      collection = new ymaps.GeoObjectCollection();
      for(var i in mapData){
        placemark = new ymaps.Placemark(mapData[i].position.coordinates, {
          balloonContentHeader: mapData[i].name,
          balloonContentBody: mapData[i].body,
          balloonContentFooter: '<strong>Координаты:</strong> ' +
          mapData[i].position.coordinates[0] +
          ', '  + mapData[i].position.coordinates[1]
        });

        collection.add(placemark);
      }

      map.geoObjects.add(collection);
      map.setBounds(collection.getBounds());
      map.setZoom(zoom);
    }
  }
};
