Drupal.behaviors.yamaps_places = {
  attach: function(context, settings) {
    var ymapId = 'ymap';
    var map, placemark,
        placesSeances = settings.placesSeances;
    var mapElement = document.querySelector('#' + ymapId);

    //Если существует элемент, в который добавлять карту, то добавляем. При инициализации id удаляется.
    if(mapElement){
      ymaps.ready(init);
    }

    function init(){
      map = new ymaps.Map(ymapId, {
        center: [55.76, 37.64],
        zoom: 12
      });

      var collection = new ymaps.GeoObjectCollection();

      for(var i in placesSeances){
        var ps = placesSeances[i];
        if(ps.map){
          placemark = new ymaps.Placemark(ps.map.coordinates, {
            balloonContentHeader: ps.data.name,
            balloonContentBody: ps.data.address,
            balloonContentFooter: ps.seances.join('<br>')
          });

          collection.add(placemark);
        }
      }
      map.geoObjects.add(collection)

      map.setBounds(collection.getBounds());

      if(map.getZoom() > 20){
        map.setZoom(10);
      }
      else{
        map.setZoom(map.getZoom());
      }

      mapElement.removeAttribute('id');
    }
  }
};
