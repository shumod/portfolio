<?php

namespace Drupal\pro_content\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\pro_regions\Controller\RegionsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Route;

class ContentConverter implements ParamConverterInterface{

  /**
   * Проверияем, какие типы данных можно конвертировать
   * @param mixed $definition
   * @param string $name
   * @param \Symfony\Component\Routing\Route $route
   *
   * @return bool
   */
  public function applies($definition, $name, Route $route) {
    return
      !empty($definition['type']) &&
      in_array(
        $definition['type'],
        ['region', 'article', 'articles', 'event', 'events', 'place', 'places', 'tag']
      );
  }

  /**
   * Конвертируем данные в объекты
   * @param mixed $value
   * @param mixed $definition
   * @param string $name
   * @param array $defaults
   *
   * @return string
   */
  public function convert($value, $definition, $name, array $defaults) {
    $output = '';

    /* @var \Drupal\pro_api\CultureApi $api */
    $api = \Drupal::service('pro_api.culture');

    switch ($definition['type']){
      /*Для региона ищем термин*/
      case 'region':
        $output = RegionsController::getRegionTermFromAlias('/r/' . $value);

        if(!$output){
          throw new NotFoundHttpException();
        }
        break;

      /* Для одиночных страниц отдаём 1 материал */
      case 'article':
      case 'event':
      case 'place':
        $type = $definition['type'] . 's';

        $api->setValues([
          'type' => $type
        ]);

        $api->setQueryValues([
          'ids' => $value
        ]);

        $api->response();

        $output = $api->data->{$type}[0];
        break;

      /* Для страниц с множественными материалами отдаём сразу все материалы */
      case 'articles':
      case 'events':
      case 'places':
        $type = $definition['type'];

        $api->setValues([
          'type' => $type
        ]);

        $api->response();

        $output = $api->data->{$type};
        break;

      /*Для тэга получаем информацию*/
      case 'tag':
        $api->setDefaultValues();

        $api->setValues([
          'type' => 'tags'
        ]);

        $api->setQueryValues([
          'ids' => $value,
          'offset' => 0
        ]);

        $api->response();
        $output = $api->data->tags[0];
        break;
    }

    return $output;
  }
}