<?php

namespace Drupal\pro_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\pro_content\Controller\ContentController;

/**
 * Provides a 'EventsBlock' block.
 *
 * @Block(
 *  id = "events_block",
 *  admin_label = @Translation("Events block"),
 * )
 */
class EventsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route_match = \Drupal::routeMatch();
    $region = $route_match->getParameter('region');
    $place = $route_match->getParameter('place');

    $events = ContentController::getContent(
      ['type' => 'events'],
      [
        'locales' => $region->field_locale_id->getString(),
        'places' => $place->_id,
        'sort' => '-start',
        'limit' => 5
      ]
    );

    //TODO: вывести ссылку на подгрузку дополнительных событий

    return [
      [
        '#theme' => 'events',
        '#events' => $events
      ]
    ];
  }

}
