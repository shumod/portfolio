<?php

namespace Drupal\pro_content\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

class PlacesMenuLink extends MenuLinkDefault{

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    return 'pro_content.places';
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters() {
    $session = \Drupal::request()->getSession();
    $region_name = $session->get('region_name') ? $session->get('region_name') : 'rf';

    return ['region' => $region_name];
  }
}