<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ArticlesController.
 */
class ArticlesController extends ControllerBase{

  /**
   * Articles.
   *
   * @return string
   */
  public function articles() {
    $articles = ContentController::getContent([
      'type' => 'articles'
    ]);

    return [
      [
        '#theme' => 'articles',
        '#articles' => $articles
      ],
      [
        '#type' => 'pager',
      ]
    ];
  }
}
