<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pro_regions\Controller\RegionsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class EventsController.
 */
class EventsController extends ControllerBase {

  /**
   * Events.
   *
   * @return string
   */
  public function events($region){
    if(!$region){
      throw new NotFoundHttpException();
    }
    else{
      $locale = $region->field_locale_id->getString();
      $events = ContentController::getContent(
        ['type' => 'events'],
        ['locales' => $locale]
      );

      return [
        [
          '#theme' => 'events',
          '#events' => $events
        ],
        [
          '#type' => 'pager',
        ]
      ];
    }
  }
}
