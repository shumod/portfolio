<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PlacesController.
 */
class PlacesController extends ControllerBase {

  /**
   * @param $region
   *
   * @return array
   */
  public function places($region){
    if(!$region){
      throw new NotFoundHttpException();
    }
    else{
      $locale = $region->field_locale_id->getString();
      $places = ContentController::getContent(
        ['type' => 'places'],
        ['locales' => $locale]
      );

      return [
        [
          '#theme' => 'places',
          '#places' => $places
        ],
        [
          '#type' => 'pager',
        ]
      ];
    }
  }
}
