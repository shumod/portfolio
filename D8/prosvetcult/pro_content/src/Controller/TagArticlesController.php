<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;

class TagArticlesController extends ControllerBase{

  public function title($tag) {
    return 'Статьи с тэгом "' . $tag->name . '"';
  }

  public function articles($tag){
    $articles = ContentController::getContent(
      ['type' => 'articles'],
      ['tags' => $tag->_id]
    );

    return [
      [
        '#theme' => 'articles',
        '#articles' => $articles
      ],
      [
        '#type' => 'pager',
      ]
    ];
  }
}