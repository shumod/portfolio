<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pro_regions\TwigExtension\RegionTwigExtension;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OneEventController.
 */
class OneEventController extends ControllerBase {

  /**
   * @param $content
   */
  public function title($event) {
    return $event->name;
  }

  /**
   * Выводим содержимое события
   * @param $event
   *
   * @return array
   */
  public function event($event) {
    $places_seances = self::getEventPlacesSeances($event);

    $result_array = [
      '#theme' => 'one_event',
      '#event' => $event,
      '#price' => self::getEventPrice($event),
      '#seances' => self::getEventSeances($event),
      '#places_seances' => $places_seances
    ];

    $output['#attached']['drupalSettings']['placesSeances'] = $places_seances;

    return [$output, $result_array];
  }

  /**
   * Получить цену события
   * @param $event
   *
   * @return string
   */
  public static function getEventPrice($event){
    if($event->isFree){
      return 'Событие бесплатно';
    }
    else{
      $price = $event->price;

      if($event->maxPrice !== $event->price && $event->maxPrice){
        $price .= ' - ' . $event->maxPrice;
      }

      return $price . '₽';
    }
  }

  /**
   * Получаем сеансы события
   * @param $event
   */
  public static function getEventSeances($event){
    $seances = NULL;

    if(isset($event->seances)){
      $seances = [];
      /* @var \Drupal\Core\Datetime\DateFormatter $formatter */
      $formatter = \Drupal::service('date.formatter');

      foreach ($event->seances as $index => $seance){
        $seances[$index] = $formatter->format(substr($seance->start, 0, -3), 'pro_date_and_time');
        if(isset($seance->end)){
          $seances[$index] .= ' - ' . $formatter->format(substr($seance->end, 0, -3), 'pro_date_and_time');
        }
      }
    }

    return $seances;
  }

  /**
   * Получаем места и сеансы для этих мест
   * @param $event
   *
   * @return array
   */
  public static function getEventPlacesSeances($event){
    $output = NULL;

    //TODO: сделать вывод сеанса рядом с местом
    if(isset($event->places)){
      $output = [];
      foreach ($event->places as $i => $place){
        $full_name_array = [];
        $clear_name = '';

        if(isset($place->_id) && isset($place->name)){ //Если это место занесено в базу культуры
          $place_url = RegionTwigExtension::get_place_region_url($place);
          $clear_name = $place->name;
          $full_name_array['name'] = '<a href="'.$place_url.'/places/'.$place->_id.'" target="_blank">' . $clear_name . '</a>';
          $full_name_array['address'] = 'Адрес: ' . ContentController::getPlaceAddress($place);
        }
        else{ //Если это место НЕ занесено в базу культуры
          if(isset($place->address->comment) && $place->address->comment != ''){
            $clear_name = $place->address->comment;
            $full_name_array['name'] = '<b>'. $clear_name . '</b>';
            $full_name_array['address'] = 'Адрес: ' . ContentController::getPlaceAddress($place);
          }
          else{
            $full_name_array['address'] = ContentController::getPlaceAddress($place);
          }
        }

        $output[$i]['full_name'] = implode('. ', $full_name_array);
        $output[$i]['data'] = $full_name_array;
        $output[$i]['data']['clear_name'] = $clear_name;
        $output[$i]['seances'] = self::getEventSeances($place);

        if(isset($place->mapPosition)){
          $output[$i]['map'] = $place->mapPosition;
        }
      }
    }

    return $output;
  }

}
