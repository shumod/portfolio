<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;

class TagEventsController extends ControllerBase{

  public function title($region, $tag) {
    return 'События с тэгом "' . $tag->name . '"';
  }

  public function tagEvents($region, $tag){
    $locale_id = $region->field_locale_id->getString();
    $limit = 20;

    $events = ContentController::getContent(
      ['type' => 'events'],
      ['limit' => $limit, 'locales' => $locale_id, 'tags' => $tag->_id]
    );

    return [
      [
        '#theme' => 'events',
        '#events' => $events
      ],
      [
        '#type' => 'pager',
      ]
    ];
  }
}