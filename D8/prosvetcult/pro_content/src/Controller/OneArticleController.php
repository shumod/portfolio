<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class OneArticleController.
 */
class OneArticleController extends ControllerBase {

  /**
   * @param $content
   */
  public function title($article) {
    return $article->name;
  }

  public function article($article) {
    return
      [
        '#theme' => 'one_article',
        '#article' => $article,
        '#description' => ContentController::getDescription($article),
        '#images' => ContentController::getImages($article),
        '#regionUrl' => ContentController::getRegionUrl($article)
      ];
  }
}
