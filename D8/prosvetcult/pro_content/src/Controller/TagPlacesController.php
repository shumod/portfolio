<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;

class TagPlacesController extends ControllerBase{

  public function title($region, $tag) {
    return 'Места с тэгом "' . $tag->name . '"';
  }

  public function tagPlaces($region, $tag){
    $locale_id = $region->field_locale_id->getString();
    $limit = 20;

    $places = ContentController::getContent(
      ['type' => 'places'],
      ['limit' => $limit, 'locales' => $locale_id, 'tags' => $tag->_id]
    );

    return [
      [
        '#theme' => 'places',
        '#places' => $places
      ],
      [
        '#type' => 'pager',
      ]
    ];
  }
}