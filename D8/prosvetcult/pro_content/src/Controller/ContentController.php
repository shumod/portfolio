<?php

namespace Drupal\pro_content\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\pro_regions\Controller\RegionsController;
use Drupal\taxonomy\Entity\Term;

class ContentController extends ControllerBase {

  /**
   * Получает контент от api
   * @param null $data
   * @param null $query
   *
   * @return mixed
   */
  public static function getContent($data = NULL, $query = NULL) {
    /* @var \Drupal\pro_api\CultureApi $api */
    $api = \Drupal::service('pro_api.culture');
    $api->setDefaultValues();

    if(is_array($data)){
      $api->setValues($data);
    }

    if(is_array($query)){
      $api->setQueryValues($query);
    }

    $api->getTotal();
    $api->initPager()->response();

    return $api->getData()->{$data['type']};
  }

  public static function getDescription($content) {
    foreach ($content->content as $item) {
      if($item->type === 'text'){
        return $item->text;
      }
    }

    return NULL;
  }

  /**
   * Перебирает контент и отдаёт картинки
   * @param $content
   * @return null
   */
  public static function getImages($content) {
    foreach ($content->content as $item) {
      if($item->type === 'images'){
        return $item->images;
      }
    }

    return NULL;
  }

  /**
   * Отдаёт урл региона
   * @param $content
   * @return \Drupal\Core\GeneratedUrl|string
   */
  public static function getRegionUrl($content) {
    $id = 1;
    foreach ($content->localeIds as $cid => $localeId){
      if($localeId === 1){
        $id = $content->localeIds[$cid - 1];
      }
    }

    $term_id = RegionsController::getTermIdFromCultureId($id);
    $term = Term::load($term_id);

    return $term->toUrl()->toString();
  }

  public static function getRegionUrlFromSubordinationIds($sub_ids){
    $i = 1;
    if(sizeof($sub_ids) > 1){
      foreach ($sub_ids as $id => $sub_id){
        if($sub_id === 1){
          $i = $sub_ids[$id - 1];
        }
      }
    }

    $term_id = RegionsController::getTermIdFromCultureId($i);
    $term = Term::load($term_id);

    return $term->toUrl()->toString();
  }

  /**
   * Отдаёт адрес места
   * @param $place
   */
  public static function getPlaceAddress($place){
    $address = [];
    $pa = $place->address;

    if(isset($pa->region)){
      $address[] = $pa->region->name . ' ' . $pa->region->type;
    }

    if(isset($pa->city->name)){
      if(isset($pa->city->type)){
        $address[] = $pa->city->type . ' ' . $pa->city->name;
      }
      else{
        $address[] = $pa->city->name;
      }
    }

    if(isset($pa->street->name)){
      if(isset($pa->street->type)){
        $address[] = $pa->street->type . ' ' . $pa->street->name;
      }
      else{
        $address[] = $pa->street->name;
      }
    }

    if(isset($pa->house->name)){
      if(isset($pa->house->type)){
        $address[] = $pa->house->type . ' ' . $pa->house->name;
      }
      else{
        $address[] = $pa->house->name;
      }
    }
    return sizeof($address) ? implode(', ', $address) : NULL;
  }

  /**
   * Отдаёт контакты места в виде массива
   * @param $place
   * @return array|null
   */
  public static function getPlaceContacts($place){
    if(isset($place->contacts)){
      $output = [];

      foreach ($place->contacts as $type => $contact){
        $attr = [
          'attributes' =>
            [
              'class' => 'contact-type contact-type-' . $type,
              'target' => '_blank'
            ]
        ];
        switch ($type){
          case 'website':
            $output[] = 'Сайт: ' . Link::fromTextAndUrl(
              $contact,
              Url::fromUri($contact, $attr)
              )->toString();
            break;

          case 'vkontakte':
            $output[] = 'ВКонтакте: ' . Link::fromTextAndUrl(
                $contact,
                Url::fromUri($contact, $attr)
              )->toString();
            break;

          case 'facebook':
            $output[] = 'Facebook: ' . Link::fromTextAndUrl(
                $contact,
                Url::fromUri($contact, $attr)
              )->toString();
            break;

          case 'youtube':
            $output[] = 'Youtube: ' . Link::fromTextAndUrl(
                $contact,
                Url::fromUri($contact, $attr)
              )->toString();
            break;

          case 'email':
            $output[] = 'E-mail: ' .
              '<a href="mailto:'.$contact.'" class="contact-type contact-type-'.$type.'">'.$contact.'</a>';
            break;

          case 'twitter':
            $output[] = 'Twitter: ' . Link::fromTextAndUrl(
                $contact,
                Url::fromUri($contact, $attr)
              )->toString();
            break;

          case 'instagram':
            $output[] = 'Instagram: ' . Link::fromTextAndUrl(
                $contact,
                Url::fromUri($contact, $attr)
              )->toString();
            break;

          case 'phones':
            $phones = '<ul>';
            foreach ($contact as $phone) {
              $comment = '';
              if($phone->comment !== ''){
                $comment = ' (' . $phone->comment . ')';
              }
              $phones .= '<li><a href="tel:'.$phone->value.'">' . $phone->value . $comment . '</a></li>';
            }
            $phones .= '</ul>';

            $output[] = 'Телефоны: ' . $phones;

            break;
        }
      }

      return sizeof($output) > 0 ? $output : NULL;
    }

    return NULL;
  }

  /**
   * Отдаёт расписание места
   * @param $place
   * @return array|null
   */
  public static function getPlaceSchedule($place){
    $schedule = [];

    if($place->workingSchedule){
      foreach ($place->workingSchedule as $day => $item) {
        $day_name = '';
        switch ($day){
          case 0: $day_name = 'Понедельник'; break;
          case 1: $day_name = 'Вторник'; break;
          case 2: $day_name = 'Среда'; break;
          case 3: $day_name = 'Четверг'; break;
          case 4: $day_name = 'Пятница'; break;
          case 5: $day_name = 'Суббота'; break;
          case 6: $day_name = 'Воскресенье'; break;
        }

        $string = [
          $day_name . ':',
          gmdate("H:i", substr($item->from, 0, -3)),
          '—',
          gmdate("H:i", substr($item->to, 0, -3))
        ];

        $schedule[] = implode(' ', $string);
      }
    }

    return sizeof($schedule) > 0 ? $schedule : NULL;
  }
}