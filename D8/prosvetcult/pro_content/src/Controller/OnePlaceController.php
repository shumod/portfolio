<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class OnePlaceController.
 */
class OnePlaceController extends ControllerBase {

  /**
   * @param $place
   *
   * @return mixed
   */
  public function title($place) {
    return $place->name;
  }

  /**
   * @param $region
   * @param $place
   *
   * @return mixed
   */
  public function place($region, $place) {
    $contacts = ContentController::getPlaceContacts($place);
    $address = ContentController::getPlaceAddress($place);

    $output = [
      [
        '#theme' => 'one_place',
        '#place' => $place,
        '#address' => $address,
        '#contacts' => $contacts,
        '#schedule' => ContentController::getPlaceSchedule($place),
      ]
    ];

    $js_body = [];
    if(sizeof($contacts) > 0){
      $js_body[] = '<strong>Контакты</strong>';
      $js_body[] = implode('<br>', $contacts) . '<br>';
    }

    $js_body[] = '<strong>Адрес</strong>';
    $js_body[] = $address;

    $output['#attached'] = [
      'drupalSettings' => [
        'map' => [
          0 => [
            'name' => $place->name,
            'body' => implode('<br>', $js_body),
            'position' => $place->mapPosition
          ]
        ]
      ]
    ];

    return $output;
  }
}
