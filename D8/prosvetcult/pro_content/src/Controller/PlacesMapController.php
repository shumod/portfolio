<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class PlacesController.
 */
class PlacesMapController extends ControllerBase {

  /**
   * @param $region
   *
   * @return array
   */
  public function map($region){
    $response = new AjaxResponse();

    $response->addCommand(new OpenModalDialogCommand(
      'title',
      'content'
    ));

    $response->setData([
      'test' => 123
    ]);
    return $response;
  }
}
