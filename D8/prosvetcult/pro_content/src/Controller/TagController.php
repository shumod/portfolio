<?php

namespace Drupal\pro_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

class TagController extends ControllerBase{

  public function title($region, $tag) {
    return $region->getName() . ': События и Места с тэгом "' . $tag->name . '"';
  }

  public function tag($region, $tag){
    $output = [];
    $pager = FALSE;
    $locale_id = $region->field_locale_id->getString();
    $locale_path = $region->field_locale_path->getString();
    $limit = 5;

    $events = ContentController::getContent(
      ['type' => 'events', 'pager' => $pager],
      ['limit' => $limit, 'locales' => $locale_id, 'tags' => $tag->_id]
    );
    $output['events'] = [
      [
        '#theme' => 'events',
        '#events' => $events,
        '#more_link' => [
          '#markup' => Link::fromTextAndUrl(
            'Все события',
            Url::fromRoute(
              'pro_content.tag_events_all',
              ['region' => $locale_path, 'tag' => $tag->_id],
              ['attributes' => ['class' => 'all-content-link']]
            )
          )->toString()
        ]
      ],
    ];

    $places = ContentController::getContent(
      ['type' => 'places', 'pager' => $pager],
      ['locales' => $locale_id, 'limit' => $limit, 'tags' => $tag->_id]
    );
    $output['places'] = [
      [
        '#theme' => 'places',
        '#places' => $places,
        '#more_link' => [
          '#markup' => Link::fromTextAndUrl(
            'Все места',
            Url::fromRoute(
              'pro_content.tag_places_all',
              ['region' => $locale_path, 'tag' => $tag->_id],
              ['attributes' => ['class' => 'all-content-link']]
            )
          )->toString()
        ]
      ]
    ];

    return [
      '#theme' => 'tag',
      '#events' => $output['events'],
      '#places' => $output['places']
    ];
  }
}