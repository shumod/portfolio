<?php
namespace Drupal\pro_content\TwigExtension;

use Drupal\Component\Utility\Unicode;

class Truncate extends \Twig_Extension {
  /**
   * Generates a list of all Twig filters that this extension defines.
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('truncate', array($this, 'truncate')),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   */
  public function getName() {
    return 'pro_content.twig_truncate';
  }

  /**
   * Replaces all numbers from the string.
   */
  public static function truncate($string, $max_length) {
    return Unicode::truncate($string, $max_length, TRUE, TRUE);
  }
}