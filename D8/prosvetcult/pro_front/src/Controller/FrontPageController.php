<?php

namespace Drupal\pro_front\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\taxonomy\Entity\Term;

/**
 * Class FrontPageController.
 */
class FrontPageController extends ControllerBase {

  /**
   * Для главной страницы отдаём все термины 2-го уровня из словаря с локалями
   * @return string
   */
  public function build() {
    $locales_tree = [];
    $locales = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree('locales', 1, 1);

    /* @var \Drupal\taxonomy\Entity\Term $locale */
    foreach ($locales as $locale){
      $first_letter = mb_substr($locale->name, 0, 1);
      $locale_object = Term::load($locale->tid);
      $locales_tree[$first_letter][$locale_object->id()] = $locale_object->toLink();
    }

    return [
      '#theme' => 'front_regions',
      '#locales' => $locales_tree
    ];
  }

}
