<?php

namespace Drupal\pro_front\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;

/**
 * Class LoadLocalesController.
 */
class LoadLocalesController extends ControllerBase {

  /**
   * Подгружаем детей выбранного термина
   * @return string
   */
  public function load() {
    $locales_list = [];
    $parent_id = \Drupal::request()->query->get('parent_id');
    $response = new AjaxResponse();

    if(!is_numeric($parent_id)){
      $response->addCommand(new OpenModalDialogCommand('Ошибка!', 'Некорректный номер региона' . $parent_id));
    }
    else{
      $locales = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree('locales', $parent_id, 1);

      /* @var \Drupal\taxonomy\Entity\Term $locale */
      foreach ($locales as $locale){
        $locale_object = Term::load($locale->tid);
        $locales_list[] = $locale_object->toLink()->toString();
      }

      $list = [
        '#theme' => 'item_list',
        '#list_type' => 'ul', // or #list
        '#items' => $locales_list,
        '#attributes' => [
          'class' => 'sublocales-list'
        ]
      ];

      $response->addCommand(new AppendCommand('.locale-' . $parent_id, $list));
      $response->addCommand(new RemoveCommand('.locale-' . $parent_id . ' .load-sublocales'));
    }

    return $response;
  }

}
