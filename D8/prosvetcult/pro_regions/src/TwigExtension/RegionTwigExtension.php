<?php

namespace Drupal\pro_regions\TwigExtension;

use Drupal\Core\Template\TwigExtension;
use Drupal\pro_content\Controller\ContentController;

class RegionTwigExtension extends TwigExtension {

  /**
   * Имя расширения для Twig
   * {@inheritdoc}
   */
  public function getName() {
    return 'pro_regions.get_region_url';
  }

  /**
   * Определяем, какие функции будут доступны
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('get_event_region_url', [$this, 'get_event_region_url']),
      new \Twig_SimpleFunction('get_place_region_url', [$this, 'get_place_region_url']),
    ];
  }

  /**
   * Получаем регион из объекта события.
   * @param $event
   * @return \Drupal\Core\GeneratedUrl|string
   */
  public static function get_event_region_url($event){
    return ContentController::getRegionUrlFromSubordinationIds($event->organization->subordinationIds);
  }

  /**
   * Получаем регион из объекта места
   * @param $place
   * @return \Drupal\Core\GeneratedUrl|string
   */
  public static function get_place_region_url($place){
    return ContentController::getRegionUrlFromSubordinationIds($place->localeIds);
  }
}