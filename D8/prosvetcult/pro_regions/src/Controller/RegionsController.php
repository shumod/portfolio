<?php

namespace Drupal\pro_regions\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RegionsController.
 */
class RegionsController extends ControllerBase {

  /**
   * Regions.
   *
   * @return string
   */
  public function regions() {
    return [
      '#type' => 'markup',
      '#markup' => 'regions'
    ];
  }

  public static function getTermFromCultureId($culture_id) {
    $term_id = self::getTermIdFromCultureId($culture_id);
    return Term::load($term_id);
  }

  /**
   * @param $culture_id
   * @return array|int
   */
  public static function getTermIdFromCultureId($culture_id){
    $tid = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'locales')
      ->condition('field_locale_id', $culture_id)
      ->execute();

    return array_shift($tid);
  }

  /**
   * Получить термин региона из алиаса пути
   * @param $alias
   * @return \Drupal\Core\Entity\EntityInterface|null|static
   */
  public static function getRegionTermFromAlias($alias) {
    $path = \Drupal::service('path.alias_manager')->getPathByAlias($alias);
    if(preg_match('/taxonomy\/term\/(\d+)/', $path, $matches)) {
      return Term::load($matches[1]);
    }
    return NULL;
  }

  /**
   * @param $region_id
   * @return null
   */
  public static function getRegionParentId($region_id) {
    $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($region_id);

    if($parent = reset($parent)){
      return is_numeric($parent->id()) ? $parent->id() : NULL;
    }

    return NULL;
  }
}
