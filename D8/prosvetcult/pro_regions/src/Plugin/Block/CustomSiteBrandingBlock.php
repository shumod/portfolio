<?php

namespace Drupal\pro_regions\Plugin\Block;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\pro_regions\Controller\RegionsController;
use Drupal\system\Plugin\Block\SystemBrandingBlock;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a 'CustomSiteBrandingBlock' block.
 *
 * @Block(
 *  id = "custom_site_branding_block",
 *  admin_label = @Translation("Custom site branding block"),
 * )
 */
class CustomSiteBrandingBlock extends SystemBrandingBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $site_name = [];
    $site_config = $this->configFactory->get('system.site');

    $site_name[] = Link::fromTextAndUrl(
      $site_config->get('name'),
      Url::fromRoute('<front>')
    )->toString();

    $session = \Drupal::request()->getSession();
    $region_name = $session->get('region_name', 'rf');
    $region_term = RegionsController::getRegionTermFromAlias('/r/' . $region_name);

    if($region_term){
      $parent_term_id = RegionsController::getRegionParentId($region_term->id());
      if($parent_term_id && $parent_term_id != 1){
        $parent_term = Term::load($parent_term_id);
        $site_name[] = $parent_term->toLink()->toString();
      }

      $site_name[] = $region_term->toLink()->toString();
    }

    $build['custom_site_branding_block'] = [
      '#theme' => 'site_branding_block',
      '#site_name' => $site_name,
      '#cache' => ['max-age' => 0]
    ];

    return $build;
  }

}
