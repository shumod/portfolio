<?php

namespace Drupal\pro_regions\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\pro_regions\Controller\RegionsController;

/**
 * Provides a 'ChangeRegionBlock' block.
 *
 * @Block(
 *  id = "change_region_block",
 *  admin_label = @Translation("Change region block"),
 * )
 */
class ChangeRegionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $change_region_link = Link::fromTextAndUrl(
      'Изменить регион',
      Url::fromRoute(
        'pro_front.front_page_controller_build',
        [],
        [
          'attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'modal'
          ]
        ]
      )
    )->toString();

    $build['change_region_block']['#markup'] = $change_region_link;
    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $build;
  }

}
