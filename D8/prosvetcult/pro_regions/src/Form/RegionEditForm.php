<?php

namespace Drupal\pro_regions\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pro_regions\Controller\RegionsController;
use Drupal\taxonomy\Entity\Term;

/**
 * Class RegionEditForm.
 */
class RegionEditForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'region_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['import_terms'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import terms'),
    ];

    $form['remove_terms'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove terms'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element_id = $form_state->getTriggeringElement()['#id'];

    /**
     * Если импорт элементов, то стучимся к апи и создаём термины.
     */
    if($triggering_element_id === 'edit-import-terms'){
      $limit = 200;

      /* @var \Drupal\pro_api\CultureApi $api */
      $api = \Drupal::service('pro_api.culture');
      $api->setValues([
        'type' => 'locales'
      ]);
      $total_locales = $api->getTotal();

      $api->setQueryValues([
        'limit' => $limit
      ]);
      $api->response();

      for($offset = 0; $offset < ceil($total_locales / $limit); $offset++){
        $api->setQueryValues(['offset' => $offset * $limit]);
        $api->response();

        foreach ($api->data->locales as $locale){
          self::createLocale($locale);
        }
      }
    } //endif

    /*
     * Удаляем все термины
     */
    else if($triggering_element_id === 'edit-remove-terms'){
      $tids = \Drupal::entityQuery('taxonomy_term')
        ->condition('vid', 'locales')
        ->execute();

      $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($tids);
      $controller->delete($entities);

      $con = \Drupal\Core\Database\Database::getConnection();
      $query = "ALTER TABLE {taxonomy_term_data} AUTO_INCREMENT = 1";
      $con->query($query);

      drupal_set_message('All terms removed');
    }
  }

  /**
   * Обновляем или создаём термин
   * @param $data
   */
  static function createLocale($data){
    $term_data_array = [
      'vid' => 'locales',
      'name' => $data->name,
      'field_locale_id' => $data->_id,
      'field_locale_timezone' => $data->timezone,
      'field_locale_path' => $data->sysName,
      'path' =>  ['alias' => '/r/' . $data->sysName]
    ];

    if(isset($data->parentId)){
      $parent_id = RegionsController::getTermIdFromCultureId($data->parentId);
      $term_data_array['parent'] = $parent_id;

      if($parent_id != 1){
        $parent_alias = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $parent_id);
        $path_alias = $parent_alias . '-' . $data->sysName;

        $term_data_array['path']['alias'] = $path_alias;
        $term_data_array['field_locale_path'] = str_replace('/r/', '', $path_alias);
      }
    }

    if(RegionsController::getTermIdFromCultureId($data->_id)){
      $term = Term::load(RegionsController::getTermIdFromCultureId($data->_id));
      self::updateLocale($term, $term_data_array);
    }
    else{
      $term = Term::create($term_data_array);
    }
    $term->save();
  }

  static function updateLocale(Term &$term, $term_data_array){
    foreach ($term_data_array as $key => $value){
      $term->set($key, $value);
    }
  }



}
