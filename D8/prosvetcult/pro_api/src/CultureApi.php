<?php

namespace Drupal\pro_api;

/**
 * Класс для работы с https://all.culture.ru/public/json/howitworks
 *
 * Class CultureApi
 * @package Drupal\pro_api
 */
class CultureApi {
  private $version, $base_url, $response_url;
  public $type, $query, $offset, $limit, $total, $pager, $data;

  public function __construct() {
    $this->setDefaultValues();
  }

  /**
   * Делаем запрос и получаем данные
   * @return $this
   */
  public function response() {
    $this->createResponseUrl();

    //Получаем данные
    $content = file_get_contents($this->response_url);
    $this->data = json_decode($content);

    return $this;
  }

  /**
   * Устанавливаем стандартные настройки
   * @return $this
   */
  public function setDefaultValues() {
    $this->version = 2.3;
    $this->base_url = 'https://all.culture.ru/api';
    $this->response_url = '';

    $this->data = [];

    $this->query = [];
    $this->offset = 0;
    $this->limit = 20;
    $this->total = 0;
    $this->pager = TRUE;
    $this->type = 'locales';

    $this->setDefaultQueryValues();

    return $this;
  }

  /**
   * Устанавливаем значения переменных
   * Допустимые переменные: offset, limit, pager, type
   * @param $data
   */
  public function setValues($data) {
    foreach ($data as $key => $value){
      $this->{$key} = $value;
    }

    return $this;
  }

  /**
   * Ставим дефолтные значения для строки параметров
   * @return $this
   */
  public function setDefaultQueryValues() {
    $this->query['limit'] = $this->limit;

    if(\Drupal::request()->query->get('page')){
      $this->setOffset();
    }

    if($this->offset){
      $this->query['offset'] = $this->offset;
    }

    return $this;
  }

  public function setOffset() {
    $this->offset = \Drupal::request()->query->get('page') * $this->limit;
  }

  /**
   * Устанавливаем параметры
   * @param $data
   * @return $this
   */
  public function setQueryValues($data) {
    foreach ($data as $key => $value){
      $this->query[$key] =  $value;

      if($key == 'limit'){
        $this->setOffset();
      }
    }

    return $this;
  }

  /**
   * Создание урла для запроса контента
   * @param $type
   * @return $this
   */
  public function createResponseUrl(){
    $url = [
      $this->base_url,
      $this->version,
      $this->type
    ];

    $this->response_url =
      implode('/', $url)
      . '?' .
      http_build_query($this->query);

    return $this;
  }

  public function getTotal() {
    $temp_limit = $this->limit;
    $this->limit = 1;

    $this->response();
    $this->limit = $temp_limit;

    return $this->data->total;
  }

  /**
   * Инициализируем пейджер
   * @return $this
   */
  public function initPager() {
    if($this->pager){
      pager_default_initialize($this->data->total, $this->limit);
    }

    return $this;
  }

  /**
   * @return mixed
   */
  public function getData() {
    return $this->data;
  }

  public function getResponseUrl() {
    return $this->response_url;
  }
}