<?php

namespace Drupal\cpayment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RobokassaConfigForm.
 */
class RobokassaConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cpayment.robokassaconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'robokassa_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cpayment.robokassaconfig');
    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Robokassa work mode'),
      '#options' => ['test' => $this->t('test'), 'dev' => $this->t('dev')],
      '#default_value' => $config->get('mode'),
    ];
    $form['login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('login'),
    ];
    $form['password_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password 1'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('password_1'),
    ];
    $form['password_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password 2'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('password_2'),
    ];
    $form['test_password_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test password 1'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('test_password_1'),
    ];
    $form['test_password_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test password 2'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('test_password_2'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cpayment.robokassaconfig')
      ->set('mode', $form_state->getValue('mode'))
      ->set('login', $form_state->getValue('login'))
      ->set('password_1', $form_state->getValue('password_1'))
      ->set('password_2', $form_state->getValue('password_2'))
      ->set('test_password_1', $form_state->getValue('test_password_1'))
      ->set('test_password_2', $form_state->getValue('test_password_2'))
      ->save();
  }

}
