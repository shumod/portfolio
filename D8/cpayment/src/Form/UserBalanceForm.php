<?php

namespace Drupal\cpayment\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cpayment\Controller\BalanceController;
use Drupal\cpayment_entity\Entity\CpaymentOperation;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class UserBalanceForm.
 */
class UserBalanceForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cpayment.userbalance',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_balance_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $message = \Drupal::request()->query->get('message');
    if($message){
      drupal_set_message($message);
    }

    $config = \Drupal::config('games_config.prices');

    $text = '
      <h4>Твой баланс: '.BalanceController::getUserBalanceWithCurrency(\Drupal::currentUser()->id()).'</h4>
      <ul>
        <li>При пополнении баланса на 1000'.$config->get("ru_site_currency").' - бонус на счёт в размере 500'.$config->get("ru_site_currency").'.</li>
        <li>Безлимитный доступ к игре в течении месяца за пополнение в размере 10 000'.$config->get("ru_site_currency").'.</li>
        <li>Пополнение баланса работает <strong>только для одиночных игр</strong>. Для групповых игр необходимо покупать билет.</li>
      </ul>
    ';

    $form['text'] = array(
      '#markup' => $text
    );

    $form['redirect_url'] = array(
      '#type' => 'hidden',
      '#value' => \Drupal::request()->query->get('redirect_url')
    );

    $form['price'] = array(
      '#type' => 'number',
      '#title' => $this->t('Fund your account'),
      '#min' => 1,
      '#size' => 30,
      '#default_value' => 500,
      '#field_suffix' => '₽',
      '#required' => true,
      '#description' => '500₽ на баланс. Без бонусов.'
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Pay now'),
    );

    $form['#attached']['library'][] = 'cpayment/payment-form-help';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $redirect = $form_state->getValue('redirect_url') ?
      'internal:'.$form_state->getValue('redirect_url') : '';

    $user_balance_operation = CpaymentOperation::create([
      'type' => 'balance_operation',
      'name' => 'Пополнение баланса',
      'field_cpayment_operation_amount' => $form_state->getValue('price'),
      'field_cpayment_operation_status' => CPAYMENT_OPERATION_IN_PROCESS,
      'field_cpayment_sucess_redirect' => $redirect
    ]);

    $user_balance_operation->save();
  }

  /**
   * {@inheritdoc}
   */
  public function access(){

    $current_uid = \Drupal::currentUser()->id();

    if($current_uid == 0){
      throw new AccessDeniedHttpException();
    }

    $url_user = \Drupal::routeMatch()->getParameter('user');
    $url_uid = $url_user;

    if(is_object($url_user)){
      $url_uid = $url_user->id();
    }

    if($current_uid == $url_uid  || $current_uid == 1){
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  public function title(){
    return $this->t('Balance(@balance)', array('@balance' => BalanceController::getUserBalanceWithCurrency(\Drupal::currentUser()->id())));
  }

}
