<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 14.01.2018
 * Time: 16:03
 */

namespace Drupal\cpayment\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\cpayment\Controller\BalanceController;

class CpaymentMenuLink extends MenuLinkDefault {
  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    //$uid = \Drupal::currentUser()->id();
    //$balance = BalanceController::getUserBalanceWithCurrency($uid);
    return parent::getTitle();
  }
}