<?php

namespace Drupal\cpayment\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\cpayment\Controller\BalanceController;

/**
 * Provides a 'BalanceBlockForUserProfile' block.
 *
 * @Block(
 *  id = "balance_block_for_user_profile",
 *  admin_label = @Translation("Balance block for user profile"),
 * )
 */
class BalanceBlockForUserProfile extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $uid = \Drupal::routeMatch()->getParameter('user')->id();
    $user_balance = BalanceController::getUserBalanceWithCurrency($uid);

    $add_to_balance_link = Link::createFromRoute(
      'Пополнить',
      'cpayment.user_balance_form',
      ['user' => $uid],
      ['attributes' => ['class' => 'add-to-balance-link']])->toString();

    $markup = 'Твой баланс: <span>' . $user_balance . '</span>' . $add_to_balance_link;

    $build['balance_block_for_user_profile']['#markup'] = $markup;

    return $build;
  }

}
