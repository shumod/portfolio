<?php

namespace Drupal\cpayment\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\cpayment\Controller\BalanceController;

/**
 * Provides a 'BalanceBlock' block.
 *
 * @Block(
 *  id = "balance_block",
 *  admin_label = @Translation("Block at balance page"),
 * )
 */
class BalanceBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $balance_url = Url::fromRoute(
      'cpayment.user_balance_form',
      array('user' => $url_uid = \Drupal::routeMatch()->getParameter('user'))
    );
    $balance_link = Link::fromTextAndUrl($this->t('Fund your account'), $balance_url)->toRenderable();

    $history_url = Url::fromRoute(
      'cpayment.user_balance_history',
      array('user' => $url_uid = \Drupal::routeMatch()->getParameter('user'))
    );
    $history_link = Link::fromTextAndUrl($this->t('Balance history'), $history_url)->toRenderable();

    $markup = render($balance_link) . ' ' . render($history_link);

    $build['balance_block']['#markup'] = $markup;

    $build['#cache']['max-age'] = 0;

    return $build;
  }

}
