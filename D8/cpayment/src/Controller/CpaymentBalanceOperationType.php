<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 16.03.2018
 * Time: 12:00
 */

namespace Drupal\cpayment\Controller;

use Drupal\cpayment_entity\Entity\CpaymentBalance;

class CpaymentBalanceOperationType extends AbstractCpaymentOperationType {

  /*
   * На создании операции, если статус оплачно - апдейт баланса пользователя.
   */
  public function insert() {
    parent::insert();

    if($this->status == 6){
      $this->updateUserBalance();
    }
  }

  /*
   * Записываем результат расчётов
   */
  public function setCheckoutResult($query) {
    parent::setCheckoutResult($query);

    $this->addBonus();
    $this->entity->save();

    $this->updateUserBalance();
  }

  /*
   * Добавляем бонус
   */
  private function addBonus() {
    $summ = (int)$this->entity->field_cpayment_operation_amount->getString();
    $bonus = $this->calculateBonus($summ);
    $this->entity->set('field_cpayment_operation_bonus', $bonus);
  }

  /*
   * Считаем бонус
   */
  private function calculateBonus($summ){
    if($summ > 0){
      return 500 * floor($summ / 1000);
    }

    return 0;
  }

  /*
   * Отдаём сущность баланса юзера
   */
  private function getUserBalance(){
    $query = \Drupal::entityQuery('cpayment_balance')
      ->condition('type', 'gamer_balance')
      ->condition('user_id', $this->entity->getOwnerId());
    $total_entity_nids = $query->execute();

    if(sizeof($total_entity_nids) == 0){
      $balance = $this->createUserBalance();
    }
    else{
      $id = array_shift($total_entity_nids);
      $balance = CpaymentBalance::load($id);
    }

    return $balance;
  }

  /*
   * Создаём баланс юзера
   */
  private function createUserBalance(){
    $uid = $this->entity->getOwnerId();

    $balance = CpaymentBalance::create([
      'name' => 'User ' . $uid . ' balance',
      'user_id' => $uid,
      'type' => 'gamer_balance',
      'status' => 0
    ]);

    $balance->save();

    return $balance;
  }

  /*
   * Обновляем баланс юзера
   */
  private function updateUserBalance(){
    $balance = $this->getUserBalance();

    $summ = (int)$this->entity->field_cpayment_operation_amount->getString();
    $bonus = (int)$this->entity->field_cpayment_operation_bonus->getString();

    $current_balance = (int)$balance->field_balance_total_amount->getString();
    $total_balance = $current_balance + $summ + $bonus;

    $balance->set('field_balance_total_amount', $total_balance);
    $balance->save();
  }

}