<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 16.03.2018
 * Time: 14:26
 */

namespace Drupal\cpayment\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\cpayment_entity\Entity\CpaymentOperation;
use Symfony\Component\HttpFoundation\Response;

class CheckoutController extends ControllerBase {

  public function __construct() {
    $this->all_query = \Drupal::request()->query->all();
    $this->operation_nid = \Drupal::request()->query->get('inv_id');
    $this->operation_entity = CpaymentOperation::load($this->operation_nid);

    $this->config = $this->config('cpayment.robokassaconfig');
  }

  public function checkoutResult() {
    $cpayment = new Cpayment($this->operation_entity);
    $cpayment->type->setCheckoutResult($this->all_query);

    $build = [
      '#markup' => t('Checkout result'),
    ];
    return $build;
  }

  /*
   * Страница выводит успешное завершение
   */
  public function checkoutSuccessPage() {
    $build = [];
    $url = $this->getRedirectUrl($this->operation_entity);

    $build['#attached']['drupalSettings']['cpayment']['redirectAfterCheckout']['url'] = $url;
    $build['#attached']['library'][] = 'cpayment/redirect-after-checkout';

    if($this->config->get('mode') == 'test'){
      $this->checkoutResult();
    }
    else{
      $build['#markup'] = $this->t('<p id="timer"></p>');
    }

    return $build;
  }

  /*
 * Контроллер для неуспешной оплаты
 */
  public function checkoutFailPage(){
    $cpayment = new Cpayment($this->operation_entity);
    $cpayment->type->setCheckoutFail($this->all_query);

    $build = [
      '#markup' => t('Payment fail'),
    ];
    return $build;
  }

  public function getRedirectUrl($entity) {
    if($entity->field_cpayment_sucess_redirect->uri){
      $url = Url::fromUri($entity->field_cpayment_sucess_redirect->uri)->toString();
    }
    else{
      switch($entity->bundle()){
        case 'balance_operation':
          $url =
            Url::fromRoute(
              'cpayment.user_balance_history',
              ['user' => \Drupal::currentUser()->id()]
            )->toString();
          break;

        case 'ticket_operation':
          $url = Url::fromRoute('view.games_group.page_1')->toString();
          break;
      }
    }
    return $url;
  }

}