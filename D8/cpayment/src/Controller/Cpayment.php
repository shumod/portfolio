<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 16.03.2018
 * Time: 11:57
 */

namespace Drupal\cpayment\Controller;


use Drupal\cpayment_entity\Entity\CpaymentOperation;

class Cpayment {
  public $type;

  /*
   * При конструировании передаём сущность и сразу делаем с ней какие-то действия
   */
  public function __construct($entity = NULL) {
    if($entity && $entity->getEntityTypeId() == 'cpayment_operation'){
      switch ($entity->bundle()){
        case 'balance_operation':
          $this->type = new CpaymentBalanceOperationType($entity);
          break;

        case 'ticket_operation':
          $this->type = new CpaymentTicketOperationType($entity);
          break;

        case 'ticket_operation_for_gamer':
          $this->type = new CpaymentTicketOperationForGamerType($entity);
          break;
      }
    }
    else{
      drupal_set_message('Сущность не того типа!', 'error');
    }
  }
}