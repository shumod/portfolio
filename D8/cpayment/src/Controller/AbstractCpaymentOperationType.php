<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 16.03.2018
 * Time: 12:06
 */

namespace Drupal\cpayment\Controller;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

abstract class AbstractCpaymentOperationType {
  public $entity;
  public $status;

  public function __construct($entity) {
    $this->entity = $entity;
    $this->status = (int)$entity->field_cpayment_operation_status->getString();
  }

  public function view() {
    if($this->status == 5){
      $this->goToCheckout();
    }
  }

  public function goToCheckout() {
    $robokassa = \Drupal::service('cpayment.robokassa');
    $robokassa->OutSum = (int)$this->entity->field_cpayment_operation_amount->getString();
    $robokassa->InvId = $this->entity->id();
    $robokassa->Desc = $this->entity->name->getString();

    $response = new TrustedRedirectResponse($robokassa->getRedirectURL());
    $response->send();
  }

  /*
   * После добавления сущности - отправляемся на её просмотр
   */
  public function insert() {
    if($this->status == 5){
      $redirect = RedirectResponse::create($this->entity->toUrl()->toString());
      $redirect->send();
    }
  }

  public function setCheckoutResult($query) {
    $this->entity->set('field_cpayment_operation_status', 6);
    $this->entity->set('field_cpayment_result', json_encode($query));
    $this->entity->save();
  }


  public function setCheckoutFail($query) {
    $this->entity->set('field_cpayment_operation_status', 7);
    $this->entity->set('field_cpayment_result', json_encode($query));
    $this->entity->save();
  }

}