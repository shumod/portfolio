<?php

namespace Drupal\cpayment\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\cpayment_entity\Entity\CpaymentBalance;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class BalanceController.
 */
class BalanceController extends ControllerBase{

  /*
   * Отдаёт баланс юзера
   */
  public static function getUserBalance($uid = 0){
    if(!$uid){
      $uid = \Drupal::currentUser()->id();
    }

    $money = 0;

    $query = \Drupal::entityQuery('cpayment_balance')
      ->condition('type', 'gamer_balance')
      ->condition('user_id', $uid);
    $total_entity_nids = $query->execute();

    if(sizeof($total_entity_nids) > 0){
      $id = array_shift($total_entity_nids);
      $balance = CpaymentBalance::load($id);

      $money = (int)$balance->field_balance_total_amount->getString();
    }

    return $money;
  }

  /**
   * Добавляет к балансу валюту пользователя
   * @param $uid
   *
   * @return string
   */
  public static function getUserBalanceWithCurrency($uid){
    return BalanceController::getUserBalance($uid) . '₽';
  }

  /**
   * Отдаёт историю
   * @return mixed
   */
  public function history(){
    $content['#markup'] = '';
    return $content;
  }

  /**
   * Редиректит на баланс пользователя
   */
  public function balance(){
    $current_uid = \Drupal::currentUser()->id();
    if($current_uid > 0){
      $response = new RedirectResponse(
        Url::fromRoute('cpayment.user_balance_form', ['user' => $current_uid])
          ->toString());
      $response->send();
    }
    else{
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * Отдаёт баланс пользователя по урлу
   */
  public static function getUserBalanceResponse(){
    $balance = self::getUserBalance();
    $balance_with_currency = self::getUserBalanceWithCurrency(\Drupal::currentUser()->id());

    return new JsonResponse(compact('balance', 'balance_with_currency'));
  }
}

