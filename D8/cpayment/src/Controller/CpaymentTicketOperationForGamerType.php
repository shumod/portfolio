<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.03.2018
 * Time: 20:46
 */

namespace Drupal\cpayment\Controller;


use Drupal\games_tickets\Controller\GamesTicketsCommonController;

class CpaymentTicketOperationForGamerType extends AbstractCpaymentOperationType {
  public function __construct($entity) {
    parent::__construct($entity);
  }

  /*
   * Функция сохраняет результат чекаута.
   */
  public function setCheckoutResult($query) {
    parent::setCheckoutResult($query);
    $this->assignTicketToUser();
  }

  /*
   * Присваиваем билет к определённому пользователю
   */
  public function assignTicketToUser() {
    $for_game_nid = $this->entity->field_cpayment_for_game->entity->id();
    $tickets = GamesTicketsCommonController::getFreeTickets($for_game_nid);

    $assigned = false;
    foreach ($tickets as $ticket){
      if(!$ticket->field_ticket_gamer->entity){
        $assigned = true;

        $ticket->set('field_ticket_gamer', $this->entity->getOwnerId());
        $ticket->save();
        $this->ticket = $ticket;

        $this->sendTicketToUser();

        break;
      }
    }

    if($assigned){
      drupal_set_message('Билет успешно куплен');
    }
    else{
      drupal_set_message('Билет не был куплен, так как билеты на игру закончились. Свяжитесь с нами для выяснения деталей.', 'error');
    }
  }

  public function sendTicketToUser() {
    GamesTicketsCommonController::sendTicketToUser($this->ticket->id(), \Drupal::currentUser()->id());
  }
}