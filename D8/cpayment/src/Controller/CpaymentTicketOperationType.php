<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 16.03.2018
 * Time: 12:00
 */

namespace Drupal\cpayment\Controller;

use Drupal\games_tickets\Controller\GamesTicketsCommonController;

class CpaymentTicketOperationType extends AbstractCpaymentOperationType {

  public function __construct($entity) {
    parent::__construct($entity);
  }

  /*
   * Функция сохраняет результат чекаута.
   */
  public function setCheckoutResult($query) {
    parent::setCheckoutResult($query);
    $this->addTickets();
  }

  /*
   * Добавление тикетов после чекаута.
   */
  public function addTickets() {
    $tickets_count = (int)$this->entity->field_cpayment_tickets_count->getString();
    $game = $this->entity->field_cpayment_for_game->entity->id();

    GamesTicketsCommonController::createTickets($tickets_count, $game);
  }
}