(function ($) {
  $( ".form-item-price input" ).on('input', function() {
    var price = parseInt($(this).val());
    description = $(".form-item-price .description");

    if(price <= 0 || price == ''){
      description.html('Некорректная сумма платежа');
    }
    else if(price >= 1 && price < 1000){
      description.html(price + '₽ на баланс. Без бонусов.');
    }
    else if(price >= 1000 && price < 10000) {
      thousands = parseInt(Math.floor(price / 1000));
      bonus = parseInt(thousands * 0.5 * 1000);
      gamesCount = parseInt(Math.floor((price + bonus) / 500));

      description.html(pluralize(
        gamesCount,
        '500₽ на баланс. Без бонусов.',
        'Бонус + ' + bonus + '₽ на баланс при пополнении на ' + price + '₽.',
        'Бонус + ' + bonus + '₽ на баланс при пополнении на ' + price + '₽.'
        )
      )

    }
    else if(price >= 10000){
      description.html('Безлимитный доступ к играм на 1 месяц при пополнении баланса больше, чем на 10000₽!');
    }

  });
}(jQuery));

function pluralize(n, one, few, many){
  var d = parseInt(n.toString().substr(-1));
  var output = '';
  if(n > 10 && n < 20) {
    output += many;
  }
  else if(d === 1){
    output += one;
  }
  else if(d >= 2 && d <= 4){
    output += few;
  }
  else {
    output += many;
  }
  return output;
}