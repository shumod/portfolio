// значение начальной секунды
var url;
var second = 7;

if(drupalSettings.cpayment){
  url = drupalSettings.cpayment.redirectAfterCheckout.url;
}

function tiktak(){
  if(document.getElementById){
    timer.innerHTML = 'Вы будете перенаправлены через ' + pluralize(second, 'секунду', 'секунды', 'секунд');
  }
  if(second == 0){
    timer.innerHTML = 'Перенаправляем...';
    clearInterval(timerId);
    window.location.replace(url);
  }
  second--;
}

window.onload = function() {
  window.timerId = setInterval(function() {
    tiktak();
  }, 1000);
}

function pluralize(n, one, few, many){
  var d = parseInt(n.toString().substr(-1));
  var output = n + ' ';
  if(n > 10 && n < 20) {
    output += many;
  }
  else if(d === 1){
    output += one;
  }
  else if(d >= 2 && d <= 4){
    output += few;
  }
  else {
    output += many;
  }
  return output;
}