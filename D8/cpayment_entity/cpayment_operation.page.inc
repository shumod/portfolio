<?php

/**
 * @file
 * Contains cpayment_operation.page.inc.
 *
 * Page callback for Cpayment operation entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cpayment operation templates.
 *
 * Default template: cpayment_operation.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cpayment_operation(array &$variables) {
  // Fetch CpaymentOperation Entity Object.
  $cpayment_operation = $variables['elements']['#cpayment_operation'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
