<?php

namespace Drupal\cpayment_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Cpayment balance entities.
 *
 * @ingroup cpayment_entity
 */
class CpaymentBalanceListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Cpayment balance ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\cpayment_entity\Entity\CpaymentBalance */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.cpayment_balance.edit_form',
      ['cpayment_balance' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
