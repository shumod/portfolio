<?php

namespace Drupal\cpayment_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cpayment balance entity.
 *
 * @see \Drupal\cpayment_entity\Entity\CpaymentBalance.
 */
class CpaymentBalanceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cpayment_entity\Entity\CpaymentBalanceInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cpayment balance entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published cpayment balance entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit cpayment balance entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete cpayment balance entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cpayment balance entities');
  }

}
