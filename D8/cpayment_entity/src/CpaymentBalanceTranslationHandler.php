<?php

namespace Drupal\cpayment_entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for cpayment_balance.
 */
class CpaymentBalanceTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
