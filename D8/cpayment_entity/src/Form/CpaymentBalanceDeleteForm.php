<?php

namespace Drupal\cpayment_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cpayment balance entities.
 *
 * @ingroup cpayment_entity
 */
class CpaymentBalanceDeleteForm extends ContentEntityDeleteForm {


}
