<?php

namespace Drupal\cpayment_entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Cpayment balance edit forms.
 *
 * @ingroup cpayment_entity
 */
class CpaymentBalanceForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\cpayment_entity\Entity\CpaymentBalance */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Cpayment balance.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Cpayment balance.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.cpayment_balance.canonical', ['cpayment_balance' => $entity->id()]);
  }

}
