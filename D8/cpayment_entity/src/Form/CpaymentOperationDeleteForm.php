<?php

namespace Drupal\cpayment_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cpayment operation entities.
 *
 * @ingroup cpayment_entity
 */
class CpaymentOperationDeleteForm extends ContentEntityDeleteForm {


}
