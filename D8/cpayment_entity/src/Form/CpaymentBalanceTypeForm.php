<?php

namespace Drupal\cpayment_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CpaymentBalanceTypeForm.
 */
class CpaymentBalanceTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $cpayment_balance_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $cpayment_balance_type->label(),
      '#description' => $this->t("Label for the Cpayment balance type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $cpayment_balance_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\cpayment_entity\Entity\CpaymentBalanceType::load',
      ],
      '#disabled' => !$cpayment_balance_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $cpayment_balance_type = $this->entity;
    $status = $cpayment_balance_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Cpayment balance type.', [
          '%label' => $cpayment_balance_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Cpayment balance type.', [
          '%label' => $cpayment_balance_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($cpayment_balance_type->toUrl('collection'));
  }

}
