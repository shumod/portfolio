<?php

namespace Drupal\cpayment_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Cpayment operation entities.
 *
 * @ingroup cpayment_entity
 */
class CpaymentOperationListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Cpayment operation ID');
    $header['name'] = $this->t('Name');
    $header['state'] = $this->t('Cpayment state');
    $header['bundle'] = $this->t('Bundle');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\cpayment_entity\Entity\CpaymentOperation */
    $row['id'] = Link::fromTextAndUrl(
      $entity->id(),
      Url::fromUserInput($entity->toUrl()->toString()));
    $row['name'] = $entity->label();
    $row['state'] = $entity->field_cpayment_operation_status->entity->getName();
    $row['bundle'] = $entity->bundle();

    return $row + parent::buildRow($entity);
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('id'), 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
