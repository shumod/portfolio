<?php

namespace Drupal\cpayment_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cpayment balance entities.
 *
 * @ingroup cpayment_entity
 */
interface CpaymentBalanceInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cpayment balance name.
   *
   * @return string
   *   Name of the Cpayment balance.
   */
  public function getName();

  /**
   * Sets the Cpayment balance name.
   *
   * @param string $name
   *   The Cpayment balance name.
   *
   * @return \Drupal\cpayment_entity\Entity\CpaymentBalanceInterface
   *   The called Cpayment balance entity.
   */
  public function setName($name);

  /**
   * Gets the Cpayment balance creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cpayment balance.
   */
  public function getCreatedTime();

  /**
   * Sets the Cpayment balance creation timestamp.
   *
   * @param int $timestamp
   *   The Cpayment balance creation timestamp.
   *
   * @return \Drupal\cpayment_entity\Entity\CpaymentBalanceInterface
   *   The called Cpayment balance entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cpayment balance published status indicator.
   *
   * Unpublished Cpayment balance are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cpayment balance is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cpayment balance.
   *
   * @param bool $published
   *   TRUE to set this Cpayment balance to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\cpayment_entity\Entity\CpaymentBalanceInterface
   *   The called Cpayment balance entity.
   */
  public function setPublished($published);

}
