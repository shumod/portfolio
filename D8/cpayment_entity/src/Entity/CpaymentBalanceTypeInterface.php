<?php

namespace Drupal\cpayment_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Cpayment balance type entities.
 */
interface CpaymentBalanceTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
