<?php

namespace Drupal\cpayment_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Cpayment operation type entity.
 *
 * @ConfigEntityType(
 *   id = "cpayment_operation_type",
 *   label = @Translation("Cpayment operation type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cpayment_entity\CpaymentOperationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cpayment_entity\Form\CpaymentOperationTypeForm",
 *       "edit" = "Drupal\cpayment_entity\Form\CpaymentOperationTypeForm",
 *       "delete" = "Drupal\cpayment_entity\Form\CpaymentOperationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cpayment_entity\CpaymentOperationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cpayment_operation_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "cpayment_operation",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cpayment_operation_type/{cpayment_operation_type}",
 *     "add-form" = "/admin/structure/cpayment_operation_type/add",
 *     "edit-form" = "/admin/structure/cpayment_operation_type/{cpayment_operation_type}/edit",
 *     "delete-form" = "/admin/structure/cpayment_operation_type/{cpayment_operation_type}/delete",
 *     "collection" = "/admin/structure/cpayment_operation_type"
 *   }
 * )
 */
class CpaymentOperationType extends ConfigEntityBundleBase implements CpaymentOperationTypeInterface {

  /**
   * The Cpayment operation type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Cpayment operation type label.
   *
   * @var string
   */
  protected $label;

}
