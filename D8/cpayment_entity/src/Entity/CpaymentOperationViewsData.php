<?php

namespace Drupal\cpayment_entity\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Cpayment operation entities.
 */
class CpaymentOperationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
