<?php

namespace Drupal\cpayment_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cpayment operation entities.
 *
 * @ingroup cpayment_entity
 */
interface CpaymentOperationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cpayment operation name.
   *
   * @return string
   *   Name of the Cpayment operation.
   */
  public function getName();

  /**
   * Sets the Cpayment operation name.
   *
   * @param string $name
   *   The Cpayment operation name.
   *
   * @return \Drupal\cpayment_entity\Entity\CpaymentOperationInterface
   *   The called Cpayment operation entity.
   */
  public function setName($name);

  /**
   * Gets the Cpayment operation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cpayment operation.
   */
  public function getCreatedTime();

  /**
   * Sets the Cpayment operation creation timestamp.
   *
   * @param int $timestamp
   *   The Cpayment operation creation timestamp.
   *
   * @return \Drupal\cpayment_entity\Entity\CpaymentOperationInterface
   *   The called Cpayment operation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cpayment operation published status indicator.
   *
   * Unpublished Cpayment operation are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cpayment operation is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cpayment operation.
   *
   * @param bool $published
   *   TRUE to set this Cpayment operation to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\cpayment_entity\Entity\CpaymentOperationInterface
   *   The called Cpayment operation entity.
   */
  public function setPublished($published);

}
