<?php

namespace Drupal\cpayment_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Cpayment balance type entity.
 *
 * @ConfigEntityType(
 *   id = "cpayment_balance_type",
 *   label = @Translation("Cpayment balance type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cpayment_entity\CpaymentBalanceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cpayment_entity\Form\CpaymentBalanceTypeForm",
 *       "edit" = "Drupal\cpayment_entity\Form\CpaymentBalanceTypeForm",
 *       "delete" = "Drupal\cpayment_entity\Form\CpaymentBalanceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cpayment_entity\CpaymentBalanceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cpayment_balance_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "cpayment_balance",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cpayment_balance_type/{cpayment_balance_type}",
 *     "add-form" = "/admin/structure/cpayment_balance_type/add",
 *     "edit-form" = "/admin/structure/cpayment_balance_type/{cpayment_balance_type}/edit",
 *     "delete-form" = "/admin/structure/cpayment_balance_type/{cpayment_balance_type}/delete",
 *     "collection" = "/admin/structure/cpayment_balance_type"
 *   }
 * )
 */
class CpaymentBalanceType extends ConfigEntityBundleBase implements CpaymentBalanceTypeInterface {

  /**
   * The Cpayment balance type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Cpayment balance type label.
   *
   * @var string
   */
  protected $label;

}
