<?php

namespace Drupal\cpayment_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Cpayment operation type entities.
 */
interface CpaymentOperationTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
