<?php

namespace Drupal\cpayment_entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for cpayment_operation.
 */
class CpaymentOperationTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
