<?php

/**
 * @file
 * Contains cpayment_balance.page.inc.
 *
 * Page callback for Cpayment balance entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cpayment balance templates.
 *
 * Default template: cpayment_balance.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cpayment_balance(array &$variables) {
  // Fetch CpaymentBalance Entity Object.
  $cpayment_balance = $variables['elements']['#cpayment_balance'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
